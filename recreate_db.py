import os
from project import create_app, db
from project.api.models import *
from time import sleep
if os.environ.get('FLASK_ENV') == 'development':
    from project.config import DevelopmentConfig
    current_config = DevelopmentConfig
elif os.environ.get('FLASK_ENV') == 'staging':
    from project.config import StagingConfig
    current_config = StagingConfig
else:
    from project.config import ProductionConfig
    current_config = ProductionConfig

app = create_app()
app.app_context().push()
app.config.from_object(current_config)
db.drop_all()
sleep(1)
db.create_all()
print('Recreated.')
