from flask import Flask, render_template
#from flask_admin import Admin
#from flask_admin.contrib.sqla import ModelView
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bcrypt import Bcrypt
from flask_cors import CORS
import os, subprocess
from project.config import ProductionConfig
#import logging


# file_handler = logging.FileHandler('log.txt')
# file_handler.setLevel(logging.WARNING)
login_mgr = LoginManager()
bcrypt = Bcrypt()
db = SQLAlchemy()


def create_app(script_info=None):

    app = Flask(__name__, template_folder='/home/mark/offenderbook/client/build', static_folder='/home/mark/offenderbook/client/build/static')
    app.config.from_object(ProductionConfig)
    #app.logger.addHandler(file_handler)
    #logging.getLogger('flask_cors').level = logging.DEBUG
    if not os.path.exists('/home/mark/offenderbook/project/log.txt'):
        subprocess.run('touch /home/mark/offenderbook/project/log.txt', shell=True)
    if not os.path.exists('/home/mark/offenderbook/project/gallery'):
        os.mkdir('/home/mark/offenderbook/project/gallery')
        os.mkdir('/home/mark/offenderbook/project/gallery/registered_users')
        os.mkdir('/home/mark/offenderbook/project/gallery/offenders')
        os.mkdir('/home/mark/offenderbook/project/gallery/orientation_checker')
        os.mkdir('/home/mark/offenderbook/project/gallery/gender_checks')
        os.mkdir('/home/mark/offenderbook/project/gallery/orientation_checker/check_against')
        os.mkdir('/home/mark/offenderbook/project/passport_checks')
        subprocess.run('sudo chmod 777 -R /home/mark/offenderbook/project/gallery', shell=True)
        subprocess.run('sudo chmod 777 -R /home/mark/offenderbook/project/passport_checks', shell=True)

    db.init_app(app)
    bcrypt.init_app(app)
    login_mgr.init_app(app)
    login_mgr.session_protection = "strong"
    # commander = Admin(app, name='BitcoinMe Admin')
    # from project.api.models import *
    # commander.add_view(ModelView(Commander, db.session))
    # commander.add_view(ModelView(Users, db.session))
    # commander.add_view(ModelView(Posts, db.session))
    # commander.add_view(ModelView(Comments, db.session))

    from project.api.auth import auth_blueprint
    from project.api.offenders import offenders_blueprint

    app.register_blueprint(auth_blueprint)
    app.register_blueprint(offenders_blueprint)
    CORS(app)

    @app.errorhandler(404)
    def error_handler(e):
        return render_template("index.html", token='hakuna matata')

    return app
