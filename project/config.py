import os, secrets

class BaseConfig:
    SQLALCHEMY_DATABASE_URI = 'mysql+mysqlconnector://mark:iamtheboss@localhost/offenderbook'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_SIZE = 280
    OFFENDERS_DIR = os.getcwd()+'/project/gallery/offenders'
    REGISTERED_USERS_DIR = os.getcwd()+'/project/gallery/registered_users'
    ORIENTATION_IMAGE_DIR = os.getcwd()+'/project/gallery/orientation_checker'
    PASSPORT_CHECKS_DIR = os.getcwd()+'/project/passport_checks'
    GENDER_CHECKS_DIR = os.getcwd()+'/project/gallery/gender_checks'
    FRONTAL_FACE_CASCADE = os.getcwd()+'/project/gender_logic/frontal_face_cascade.xml'
    EYE_CASCADE = os.getcwd()+'/project/gender_logic/haarcascade_eye.xml'
    REPORT_PATH = os.getcwd()+'/client/public'
    CORS_HEADERS = 'Content-Type'


class DevelopmentConfig(BaseConfig):
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    SECRET_KEY = secrets.token_hex(12)
    DEBUG_TB_ENABLED = True
    DEBUG = True


class StagingConfig(BaseConfig):
    CLOUDSQL_USER = 'root'
    CLOUDSQL_PASSWORD = 'iaminstance'
    CLOUDSQL_DATABASE = 'staging'
    CLOUD_PROJECT_ID = 'offenderbook-310908'
    SQLALCHEMY_DATABASE_URI = f'mysql+mysqldb://root:{CLOUDSQL_PASSWORD}@35.246.97.174/{CLOUDSQL_DATABASE}?unix_socket=/cloudsql/{CLOUD_PROJECT_ID}'
    SECRET_KEY = secrets.token_hex(12)
    MAX_CONTENT_LENGTH = 50 * 1024 * 1024
    DEBUG_TB_ENABLED = False
    DEBUG = False


class ProductionConfig(BaseConfig):
    CLOUDSQL_USER = 'root'
    CLOUDSQL_PASSWORD = 'iaminstance'
    CLOUDSQL_DATABASE = 'production'
    CLOUD_PROJECT_ID = 'offenderbook-310908'
    SQLALCHEMY_DATABASE_URI = f'mysql+mysqldb://root:{CLOUDSQL_PASSWORD}@35.246.97.174/{CLOUDSQL_DATABASE}?unix_socket=/cloudsql/{CLOUD_PROJECT_ID}'
    SECRET_KEY = secrets.token_hex(12)
    MAX_CONTENT_LENGTH = 50 * 1024 * 1024
    DEBUG_TB_ENABLED = False
    DEBUG = False
