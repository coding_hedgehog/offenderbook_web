from project import db, bcrypt
from sqlalchemy.sql import func
from flask_login import UserMixin


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id              = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username        = db.Column(db.String(128), nullable=False, unique=True)
    first_name      = db.Column(db.String(128), nullable=True)
    middle_name     = db.Column(db.String(128), nullable=True)
    last_name       = db.Column(db.String(128), nullable=True)
    email           = db.Column(db.String(128), nullable=False, unique=True)
    password        = db.Column(db.String(255), nullable=False)
    auth_string     = db.Column(db.String(128), nullable=False)
    admin           = db.Column(db.Boolean, default=False)
    past_misuse     = db.Column(db.Boolean, default=False)
    email_confirmed = db.Column(db.Boolean, default=False)
    active          = db.Column(db.Boolean, default=False)
    created_date    = db.Column(db.DateTime, default=func.now(), nullable=False)
    comments        = db.relationship('Comment', backref='author')

    def __init__(self, username, email, password, auth_string):
        self.username = username
        self.email = email
        self.password = bcrypt.generate_password_hash(password).decode()
        self.auth_string = auth_string

    def to_json(self):
        return {
            'id': self.id,
            'username': self.username,
            'first_name': self.first_name,
            'middle_name': self.middle_name,
            'last_name': self.last_name,
            'past_misuse': self.past_misuse
        }

    def is_admin(self):
        return self.admin

    def is_authenticated(self):
        return self.authenticated

    def is_email_confirmed(self):
        return self.email_confirmed

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id


class Offender(db.Model):
    __tablename__       = 'offenders'
    id                  = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name          = db.Column(db.String(128), nullable=True)
    middle_name         = db.Column(db.String(128), nullable=True)
    last_name           = db.Column(db.String(128), nullable=True)
    timestamp           = db.Column(db.DateTime, default=func.now())
    comments            = db.relationship('Comment', backref='owner', cascade='all, delete, delete-orphan')
    photos              = db.relationship('Photo', backref='owner', cascade='all, delete, delete-orphan')

    def __init__(self, first_name, middle_name, last_name):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name

    def to_json(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'middle_name': self.middle_name,
            'last_name': self.last_name,
            'timestamp': self.timestamp
        }


class Comment(db.Model):
    __tablename__       = 'comments'
    id                  = db.Column(db.Integer, primary_key=True, autoincrement=True)
    post                = db.Column(db.String(255), nullable=False)
    count               = db.Column(db.Integer, default=1, nullable=False)
    confirmed           = db.Column(db.Boolean, default=False)
    created             = db.Column(db.DateTime, default=func.now())
    owner_id            = db.Column(db.Integer, db.ForeignKey('offenders.id'))
    author_id           = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __init__(self, post, owner_id, author_id):
        self.post = post
        self.owner_id = owner_id
        self.author_id = author_id

    def to_json(self):
        return {
            'id': self.id,
            'post': self.post,
            'count': self.count,
            'confirmed': self.confirmed,
            'timestamp': self.created,
            'author': self.author_id
        }


class Photo(db.Model):
    __tablename__       = 'photos'
    id                  = db.Column(db.Integer, primary_key=True, autoincrement=True)
    photo_file          = db.Column(db.String(255), nullable=False)
    owner_id            = db.Column(db.Integer, db.ForeignKey('offenders.id'))

    def __init__(self, photo_file, owner_id):
        self.photo_file = photo_file
        self.owner_id = owner_id
