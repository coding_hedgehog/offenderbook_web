from flask import request, Blueprint, jsonify, escape, current_app
from flask_login import login_user, logout_user
from project import db, bcrypt, login_mgr
from project.api.models import User
from sqlalchemy import exc, or_
from werkzeug.utils import secure_filename
from secrets import token_hex
import face_recognition, logging, requests, time, json, pytesseract, cv2, os, shutil, math,\
        numpy as np, subprocess
from .countries import COUNTRIES
from PIL import Image


logging.basicConfig(filename='log.txt', level=logging.DEBUG)
pytesseract.pytesseract.tesseract_cmd = '/usr/bin/tesseract'

KEYWORDS = ['pass', 'podpis', 'drzitele', 'holder', 'signature', 'passport',
            'passeport', 'code', 'place of birth', 'port', 'given', 'birth', 'surname', 'middle',
            'type', 'nationality', 'naissance', 'nationalite', 'republic', 'identity',
            'kingdom', 'federation', 'united', 'emirates', 'russian', 'republique',
            'permis', 'signature', 'francaise', 'sex', 'italiana', 'repubblica', 'documento',
            'nacional', 'espana', 'pasaporte', 'states']

SPECIAL_CHARS = ['.', '?', '*', ',', ', ', ';', ')', '(', ':', '%', '£', '$', '&', '"', '¥', '|']

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'JPG'}

auth_blueprint = Blueprint('auth', __name__, url_prefix='/auth')


# HELPER FUNCTIONS

def checkForMatch(dir_checking_against, dir_being_checked, check_for_duplicate=False,
 checking_offenders_dir=False, single_image_check=False, check_for_account_duplicate=False):
    if check_for_duplicate and not checking_offenders_dir and not check_for_account_duplicate:
        if len(os.listdir(current_app.config['REGISTERED_USERS_DIR'])) == 0:
            return False
    elif check_for_duplicate and checking_offenders_dir:
        if len(os.listdir(current_app.config['OFFENDERS_DIR'])) == 0:
            return False
        else:
            list_of_offenders = os.listdir(current_app.config['OFFENDERS_DIR'])
            for offender in list_of_offenders:
                offender_dir = current_app.config['OFFENDERS_DIR']+f'/{offender}/first'
                output = subprocess.run(f'face_recognition --cpus -1 {offender_dir} {dir_being_checked}', capture_output=True, shell=False)
                output = str(output)
                output = output.split()
                for result in output:
                    if 'stdout' in result:
                        result_to_check = result.replace(offender_dir, '')
                        if os.listdir(offender_dir)[0].split('.')[0] in result_to_check.strip('\n'):
                            return True
            return False
    elif checking_offenders_dir and single_image_check:
        # breakpoint()
        output = subprocess.run(f'face_recognition --cpus -1 {dir_checking_against} {dir_being_checked}', capture_output=True, shell=False)
        output = str(output)
        output = output.split()
        for result in output:
            if 'stdout' in result:
                result_to_check = result.replace(dir_being_checked, '')
                image_name_with_ext = os.listdir(dir_checking_against)[0]
                image_name = image_name_with_ext.split('.')[0]
                if image_name in result_to_check.strip('\n'):
                    return True
                else:
                    return False
    else:
        output = subprocess.run(f'face_recognition --cpus -1 --tolerance 0.85 {dir_checking_against} {dir_being_checked}', capture_output=True, shell=False)
        output = str(output)
        output = output.split()
        for result in output:
            if 'stdout' in result:
                result_to_check = result.replace(dir_being_checked, '')
                if not check_for_duplicate and not check_for_account_duplicate:
                    if 'passport' in result_to_check.strip('\n'):
                        return True
                    else:
                        return False
                elif check_for_account_duplicate:
                    if len(os.listdir(dir_checking_against)) == 0:
                        return False
                    image_files = os.listdir(dir_being_checked)
                    for img in image_files:
                        if img in result_to_check.strip('\n'):
                            return True
                        else:
                            return False


def getFaceBox(net, frame, conf_threshold=0.7):
    frameOpenCVDnn = frame.copy()
    frameHeight = frameOpenCVDnn.shape[0]
    frameWidth = frameOpenCVDnn.shape[1]
    blob = cv2.dnn.blobFromImage(frameOpenCVDnn, 1.0, (300, 300), [104, 117, 123], True, False)

    net.setInput(blob)
    detections = net.forward()
    bboxes = []
    for i in range(detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > conf_threshold:
            x1 = int(detections[0, 0, i, 3] * frameWidth)
            y1 = int(detections[0, 0, i, 4] * frameHeight)
            x2 = int(detections[0, 0, i, 5] * frameWidth)
            y2 = int(detections[0, 0, i, 6] * frameHeight)
            bboxes.append([x1, y1, x2, y2])
            cv2.rectangle(frameOpenCVDnn, (x1, y1), (x2, y2), (0, 255, 0), int(round(frameHeight / 150)), 8)
    return frameOpenCVDnn, bboxes

faceProto = '/home/mark/offenderbook/project/gender_logic/opencv_face_detector.pbtxt'
faceModel = '/home/mark/offenderbook/project/gender_logic/opencv_face_detector_uint8.pb'
genderProto = '/home/mark/offenderbook/project/gender_logic/gender_deploy.prototxt'
genderModel = '/home/mark/offenderbook/project/gender_logic/gender_net.caffemodel'

MODEL_MEAN_VALUES = (78.4263377603, 87.7689143744, 114.895847746)
genderList = ['Male', 'Female']
genderNet = cv2.dnn.readNet(genderModel, genderProto)
faceNet = cv2.dnn.readNet(faceModel, faceProto)
padding = 20

def gender_detector(img):
    frameFace, bboxes = getFaceBox(faceNet, img)
    for bbox in bboxes:
        face = img[max(0, bbox[1]-padding):min(bbox[3]+padding, img.shape[0]-1), max(0, bbox[0]-padding):min(bbox[2]+padding, img.shape[1]-1)]
        blob = cv2.dnn.blobFromImage(face, 1.0, (227, 227), MODEL_MEAN_VALUES, swapRB=False)
        genderNet.setInput(blob)
        genderPreds = genderNet.forward()
        gender = genderList[genderPreds[0].argmax()]
        return gender


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@login_mgr.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


# ----------------------------------


# ROUTES

@auth_blueprint.route('/confirm_email/<auth_string>')
def confirm_email(auth_string):
    try:
        user = User.query.filter_by(auth_string=auth_string).first()
        if user.is_email_confirmed():
            return jsonify({'status': 'fail', 'message': 'this email has been already confirmed.'}), 200
        elif user:
            user.email_confirmed = True
            db.session.commit()
            return jsonify({'status': 'success', 'message': 'email confirmed.'}), 200
    except Exception as e:
        return jsonify({'status': 'fail', 'message': str(e)}), 404


@auth_blueprint.route('/submit_feedback', methods=['POST'])
def feedback():
    response_obj = {
        'status': 'fail',
        'message': 'Something Went Wrong'
    }
    feedback = escape(request.form.get('feedback'))
    try:
        requests.post(
            "https://api.eu.mailgun.net/v3/info.offenderbook.com/messages",
            auth=("api", "35e0f48c3c3de4af6c9b34bb0f4429b7-4b1aa784-75d469fd"),
            data={"from": "offenderbook <info@offenderbook.com>",
                "to": ["marek.alexa@offenderbook.com"],
                "subject": "Feedback Sent Through Offenderbook",
                "text": f"\n\nFeedback:\n\n{feedback}"})
        response_obj['status'] = 'success'
        response_obj['message'] = 'feedback received'
    except:
        pass
    finally:
        return jsonify(response_obj), 200


@auth_blueprint.route('/check_passport', methods=['POST'])
def check_passport():
    response_obj = {
        'status': 'fail',
        'message': 'to be replaced'
    }
    try:
        image_received = request.files['image']
        is_smartphone = request.files.get('json_data')
        # print('IS SMARTPHONE ', is_smartphone)
        #breakpoint()
        filename = secure_filename(image_received.filename)
        if not allowed_file(filename):
            response_obj['message'] = 'Unsupported filetype'
            return jsonify(response_obj)
        #breakpoint()
        i = Image.open(image_received)
        if is_smartphone is None:
            if i._getexif() is None:
                i.close()
                response_obj['message'] = 'likely fake'
                return jsonify(response_obj), 200
        temp = f'passport_check_{token_hex(8)}'
        temp_dir = current_app.config["PASSPORT_CHECKS_DIR"]+f'/{temp}'
        os.mkdir(temp_dir)
        i = Image.open(image_received)
        exif_data = i._getexif()
        if exif_data is None:
            i.close()
            shutil.rmtree(temp_dir)
            response_obj['message'] = 'likely fake'
            return jsonify(response_obj), 200

        i.save(temp_dir+f'/{filename}')
        i.close()
        face_cascade = cv2.CascadeClassifier(current_app.config['FRONTAL_FACE_CASCADE'])
        image = cv2.imread(temp_dir+f'/{filename}')
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=3, minSize=(30, 30))
        found_face = len(faces)
        counter = 0

        while found_face == 0 and counter < 4:
            image = Image.open(temp_dir+f'/{filename}')
            image = image.rotate(90)
            image.save(temp_dir+f'/{filename}')
            image.close()
            result = None
            image = cv2.imread(temp_dir+f'/{filename}')
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=3, minSize=(30, 30))
            result = len(faces)
            counter += 1

        if found_face == 0 and counter > 3:
            shutil.rmtree(temp_dir)
            response_obj['message'] = 'not a passport'
            return jsonify(response_obj), 200

        img_for_gender_detection = cv2.imread(temp_dir+f'/{filename}')
        check_gender = gender_detector(img_for_gender_detection)

        if check_gender == 'Male':
            shutil.rmtree(temp_dir)
            response_obj['message'] = 'male'
            return jsonify(response_obj)
        thresh = 200
        passport_passed = False
        fn = lambda x : 255 if x > thresh else 0
        i = Image.open(temp_dir+f'/{filename}')
        img = i.convert('L').point(fn, mode='1')
        image = pytesseract.image_to_string(img, config='--psm 6')
        i.close()

        for word in KEYWORDS:
            if word.upper() in image or word.title() in image:
                passport_passed = True
                break
        if not passport_passed:
            for country in COUNTRIES:
                if country in image:
                    passport_passed = True
                    break

            if not passport_passed:
                counter += 1
                print('COUNTER IS: ', counter)
                print('ROTATING 90')
                x = x.rotate(90)
                x.save(temp_dir+f'/{filename}')
                x.close()

        if not passport_passed:
            image = pytesseract.image_to_string(img, output_type=pytesseract.Output.DICT, config='--psm 6', lang='eng')

            for word in KEYWORDS:
                if word in image:
                    passport_passed = True
                    break

            if not passport_passed:
                for country in COUNTRIES:
                    if country in image:
                        passport_passed = True
                        break
                if not passport_passed:
                    temp, list_of_el = [], image['text'].copy()
                    for el in image['text']:
                        if '/' in el:
                            a = el.split('/')
                            temp.append(a)
                        a = ''
                    for i in temp:
                        if isinstance(i, list):
                            for n in i:
                                list_of_el.append(n)
                    for word in KEYWORDS:
                        if word.upper() in list_of_el or word.title() in list_of_el:
                            passport_passed = True
                            break
                    if not passport_passed:
                        for country in COUNTRIES:
                            if country in list_of_el:
                                passport_passed = True
                                break
        if passport_passed:
            response_obj['status'] = 'OK'
            response_obj['message'] = 'passed'
            # for var in [image, thresh, i, img_for_gender_detection, threshold_img]:
            #     var = None
            shutil.rmtree(temp_dir)
            return jsonify(response_obj), 200
        else:
            response_obj['status'] = 'fail'
            response_obj['message'] = 'did not pass'
            response_obj['text'] = image['text']
            # for var in [image, thresh, i, img_for_gender_detection, threshold_img]:
            #     var = None
            shutil.rmtree(temp_dir)
            return jsonify(response_obj), 400
    except Exception as e:
        # for var in [image, thresh, i, img_for_gender_detection, threshold_img]:
        #     var = None
        img_for_gender_detection = None
        return jsonify({'status': 'fail', 'message': str(e)}), 400


@auth_blueprint.route('/register', methods=['POST'])
def register():
    response_obj = {
        'status': 'fail',
        'message': 'Invalid Payload'
    }

    # checking if data coming through the website or through mobile app
    json_data = request.files.get('json_data')
    data = json.load(json_data)

    if data is not None:
        username = escape(data['username'])
        email = escape(data['email'])
        password = escape(data['password'])
        data = None
        json_data = None
        smartphone = True
    else:
        username = escape(request.form.get('username'))
        email = escape(request.form.get('email'))
        password = escape(request.form.get('password'))
        smartphone = False
    received_passport = request.files['passport_photo']
    received_photo1 = request.files['selfie1']
    received_photo2 = request.files['selfie2']
    received_photo3 = request.files['selfie3']
    passport_filename = secure_filename(received_passport.filename)
    first_selfie_filename = secure_filename(received_photo1.filename)
    second_selfie_filename = secure_filename(received_photo2.filename)
    third_selfie_filename = secure_filename(received_photo3.filename)
    for selfie_received in [first_selfie_filename, second_selfie_filename, third_selfie_filename]:
        if not allowed_file(selfie_received):
            response_obj['message'] = 'Only image files are allowed'
            return jsonify(response_obj)

    if not smartphone:
        for img in [received_photo1, received_photo2, received_photo3]:
            i = Image.open(img)
            if i._getexif() is None:
                for x in [received_photo1, received_photo2, received_photo3]:
                    x.close()
                response_obj['message'] = 'likely fake'
                return jsonify(response_obj), 200
            i.close()


    try:
        user = User.query.filter(or_(User.username == username, User.email == email)).first()
        # user = False
        if not user:
            passed = False
            duplicate = False
            random_temp_dir = f'dir_{token_hex(8)}'
            os.mkdir(current_app.config['PASSPORT_CHECKS_DIR']+f'/{random_temp_dir}')
            os.mkdir(current_app.config['PASSPORT_CHECKS_DIR']+f'/{random_temp_dir}/selfies')
            os.mkdir(current_app.config['PASSPORT_CHECKS_DIR']+f'/{random_temp_dir}/passport_dir')
            temp_dir = current_app.config['PASSPORT_CHECKS_DIR']+f'/{random_temp_dir}'
            selfies_dir = current_app.config['PASSPORT_CHECKS_DIR']+f'/{random_temp_dir}/selfies'
            passport_dir = current_app.config['PASSPORT_CHECKS_DIR']+f'/{random_temp_dir}/passport_dir'
            received_passport.save(os.path.join(passport_dir, passport_filename))
            to_crop = Image.open(passport_dir+f'/{passport_filename}')
            width, height = to_crop.size
            half_of_the_width = width / 2
            os.remove(os.path.join(passport_dir, passport_filename))
            cropped = to_crop.crop((0, 0, half_of_the_width, half_of_the_width))
            cropped.save(os.path.join(passport_dir, passport_filename))
            received_photo1.save(os.path.join(selfies_dir, first_selfie_filename))
            received_photo2.save(os.path.join(selfies_dir, second_selfie_filename))
            received_photo3.save(os.path.join(selfies_dir, third_selfie_filename))
            for i in os.listdir(selfies_dir):
                im1 = Image.open(os.path.join(selfies_dir, i))
                width, height = im1.size
                if width > 1000:
                    os.remove(os.path.join(selfies_dir, i))
                    im1resized = im1.resize((math.floor(width/3), math.floor(height/3)))
                    im1resized.save(os.path.join(selfies_dir, i))
                im1.close()


            results = []
            face_cascade = cv2.CascadeClassifier(current_app.config['FRONTAL_FACE_CASCADE'])
            for i in os.listdir(selfies_dir):
                image = cv2.imread(selfies_dir+f'/{i}')
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=3, minSize=(30, 30))
                results.append(len(faces))

            if 0 in results:
                shutil.rmtree(temp_dir)
                response_obj['message'] = 'not a face'
                return jsonify(response_obj), 200

            for i in os.listdir(selfies_dir):
                img_for_gender_detection = cv2.imread(selfies_dir+f'/{i}')
                check_gender = gender_detector(img_for_gender_detection)
                if check_gender == 'Male':
                   shutil.rmtree(temp_dir)
                   response_obj['message'] = 'male'
                   return jsonify(response_obj), 200

            # CHECK FOR DUPLICATE FIRST
            duplicate = checkForMatch(current_app.config['REGISTERED_USERS_DIR'], selfies_dir, check_for_account_duplicate=True)
            if not duplicate:
                passed = checkForMatch(passport_dir, selfies_dir)
                # AND NOW CHECK FOR MATCH
                if not passed:
                    shutil.rmtree(temp_dir)
                    response_obj['message'] = 'Sorry. Your photo don\'t match the photo in the passport.'
                    return jsonify(response_obj), 200
                else:
                    auth_string = token_hex(12)
                    new_user = User(username=username, email=email, password=password, auth_string=auth_string)
                    db.session.add(new_user)
                    db.session.commit()
                    image_to_keep = Image.open(current_app.config['PASSPORT_CHECKS_DIR']+f'/{random_temp_dir}/selfies/{os.listdir(selfies_dir)[0]}')
                    extension = image_to_keep.filename[-3:]
                    image_to_keep.save(current_app.config['REGISTERED_USERS_DIR']+f'/{username}.{extension}', optimize=True, quality=30)
                    shutil.rmtree(temp_dir)
                    response_obj['status'] = 'success'
                    response_obj['message'] = f'{new_user.email} successfully registered'
                    response_obj['user_id'] = new_user.id
                    if smartphone:
                        text = f"Hi {username},\n\nWelcome to Offenderbook !\n\nPlease confirm your email by clicking on the link below:\n\nhttps://offenderbook.com/confirm_email_mobile/{new_user.auth_string}"
                    else:
                        text = f"Hi {username},\n\nWelcome to Offenderbook !\n\nPlease confirm your email by clicking on the link below:\n\nhttps://offenderbook.com/confirm_email/{new_user.auth_string}"
                    requests.post(
            		"https://api.eu.mailgun.net/v3/info.offenderbook.com/messages",
            		auth=("api", "35e0f48c3c3de4af6c9b34bb0f4429b7-4b1aa784-75d469fd"),
            		data={"from": "offenderbook <info@offenderbook.com>",
            			"to": [f"{email}"],
            			"subject": f"Hello, {username} !",
            			"text": text
                        })

                    requests.post(
                		"https://api.eu.mailgun.net/v3/info.offenderbook.com/messages",
                		auth=("api", "35e0f48c3c3de4af6c9b34bb0f4429b7-4b1aa784-75d469fd"),
                		data={"from": "offenderbook <info@offenderbook.com>",
                			"to": ["marek.alexa@offenderbook.com"],
                			"subject": "Account Created On Offenderbook !",
                			"text": f"User with username {username} and email {email} joined."})

                    return jsonify(response_obj), 201

            else:
                shutil.rmtree(temp_dir)
                response_obj['message'] = 'this user exists.'
                return jsonify(response_obj), 200

        else:
            response_obj['message'] = 'this user exists.'
            return jsonify(response_obj), 200
    except (exc.IntegrityError, ValueError):
        db.session.rollback()
        return jsonify(response_obj), 400


@auth_blueprint.route('/login', methods=['POST'])
def login():
    response_obj = {
        'status': 'fail',
        'message': 'Invalid Payload'
    }
    post_data = request.get_json()
    if not post_data:
        return jsonify(response_obj), 400

    username_or_email = escape(post_data.get('username_or_email'))
    password = escape(post_data.get('password'))
    #if username_or_email is None:
    #    post_data = json.loads(post_data)
    #    username_or_email = escape(post_data.get('username_or_email'))
    #    password = escape(post_data.get('password'))
    try:
        user = User.query.filter(or_(User.username == username_or_email,
        User.email == username_or_email)).first()
        if user and not user.is_email_confirmed():
            response_obj['message'] = 'confirm email first'
            return jsonify(response_obj), 200
        elif user and bcrypt.check_password_hash(user.password, password):
            response_obj['status'] = 'success'
            response_obj['message'] = 'Successfully logged in.'
            response_obj['username'] = user.username
            response_obj['user_id'] = user.id
            user.active = True
            db.session.commit()
            login_user(user)
            return jsonify(response_obj), 200
        else:
            response_obj['message'] = 'This user does not exist.'
            return jsonify(response_obj), 200
    except Exception:
        response_obj['message'] = 'Try again'
        return jsonify(response_obj), 500


@auth_blueprint.route('/get_user_data_by_id/<id>')
def return_data(id):
    user = User.query.filter_by(id=id).first()
    if user.first_name is None:
        return jsonify({'status': 'success', 'message': 'prompt_user_for_data'}), 200
    return jsonify({'status': 'success', 'message': user.to_json()}), 200


@auth_blueprint.route('/get_user_by_id/<id>')
def return_username(id):
    # print('CALLED')
    user = User.query.filter_by(id=id).first()
    # print(user)
    user.active = True
    db.session.commit()
    return jsonify({'status': 'success', 'message': user.username}), 200


@auth_blueprint.route('/logout/<id>')
def logout(id):
    if id is None:
        return jsonify({'status': 'fail', 'message': 'Noone to log out.'}), 400
    else:
        user = User.query.filter_by(id=id).first()
        logout_user()
        user.active = False
        db.session.commit()
        response_obj = {
            'status': 'success',
            'message': 'successfully logged out'
        }

        return jsonify(response_obj), 200
