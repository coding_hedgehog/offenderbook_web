from flask import (Blueprint, jsonify, json, request, send_from_directory, escape,
                    current_app)
from project.api.models import User, Offender, Comment, Photo
from .auth import allowed_file, checkForMatch, ALLOWED_EXTENSIONS, gender_detector
from werkzeug.utils import secure_filename
from werkzeug.exceptions import BadRequestKeyError
import logging, os, math, shutil, cv2, numpy as np
from secrets import token_hex
from project import db
from sqlalchemy import exc
from PIL import Image, ExifTags


offenders_blueprint = Blueprint('offenders', __name__, url_prefix='/offenders')
logging.basicConfig(filename='log.txt', level=logging.DEBUG)

@offenders_blueprint.route('/download_report')
def download_report():
    target = current_app.config['REPORT_PATH']
    return send_from_directory(target, 'domestic_violence_report.pdf', as_attachment=True)


@offenders_blueprint.route('/confirm_comment/<id>')
def confirm_comment(id):
    comment = Comment.query.filter_by(id=id).first()
    comment.confirmed = True
    comment.count = 2
    db.session.commit()
    return jsonify({'status': 'success', 'message': 'confirmed'}), 200


@offenders_blueprint.route('/submit_new_comment', methods=['POST'])
def submit_new_comment():
    # breakpoint()
    new_comment_mobile = request.get_json()
    if new_comment_mobile is None:
        # print('IF STMTS')
        new_comment = escape(request.form.get('new_comment'))
        offender_id = request.form.get('offender_id')
        user_id = request.form.get('user_id')
    else:
        new_comment = escape(new_comment_mobile.get('new_post'))
        offender_id = new_comment_mobile.get('offender_id')
        user_id = new_comment_mobile.get('user_id')
    new_warning = Comment(post=new_comment, owner_id=offender_id, author_id=user_id)
    db.session.add(new_warning)
    #print(new_warning)
    db.session.commit()
    return jsonify({'status': 'success', 'message': 'waiting for confirmation'}), 201


@offenders_blueprint.route('/add', methods=['POST'])
def add_offender():
    response_obj = {
        'status': 'fail',
        'message': 'Something Went Wrong'
    }
    # breakpoint()
    results = []
    through_smartphone = False
    image_file1 = request.files['image_file1']
    image_file2 = request.files['image_file2']
    image_file3 = request.files['image_file3']
    try:
        userID = request.form.get('userID')
        first_name = escape(request.form.get('first_name'))
        middle_name = escape(request.form.get('middle_name'))
        last_name = escape(request.form.get('last_name'))
        new_comment = escape(request.form['new_comment'])
        is_smartphone = False
    except BadRequestKeyError:
        d = request.files.get('data').read()
        data = json.loads(d)
        userID = data['userID']
        first_name = escape(data['first_name'])
        middle_name = escape(data['middle_name'])
        last_name = escape(data['last_name'])
        new_comment = escape(data['new_comment'])
        is_smartphone = True
        d = None
        data = None
    image_filename1 = secure_filename(image_file1.filename)
    image_filename2 = secure_filename(image_file2.filename)
    image_filename3 = secure_filename(image_file3.filename)

    if image_filename1 == image_filename2 or image_filename2 == image_filename3:
        splitted1 = image_filename2.split('.')
        splitted2 = image_filename3.split('.')
        filename1 = splitted1[0]
        filename2 = splitted2[0]
        ext1 = splitted1[1]
        ext2 = splitted2[1]
        image_filename2 = filename1+f'_{token_hex(2)}'+'.'+ext1
        image_filename3 = filename2+f'_{token_hex(2)}'+'.'+ext2

    for photo_received in [image_filename1, image_filename2, image_filename3]:
        if not allowed_file(photo_received):
            response_obj['message'] = 'Only image files are allowed'
            return jsonify(response_obj)

    temp = f'temp_dir_{token_hex(8)}'
    temp_dir = current_app.config['ORIENTATION_IMAGE_DIR']+f'/{temp}'
    os.mkdir(temp_dir)
    filenames = [image_filename1, image_filename2, image_filename3]

    if is_smartphone:
        for index, img in enumerate([image_file1, image_file2, image_file3]):
            i = Image.open(img)
            try:
                exif = {
                ExifTags.TAGS[k]: v
                for k, v in i._getexif().items()
                if k in ExifTags.TAGS
            }
            except AttributeError:
                for x in [image_file1, image_file2, image_file3]:
                    x.close()
                temp = None
                response_obj['message'] = 'likely fake'
                return jsonify(response_obj), 200

            #print('ORIENTATION IS: ', exif['Orientation'])
            if exif['Orientation'] != 8:
                if exif['Orientation'] == 1:
                    i = i.rotate(180)
                elif exif['Orientation'] == 6:
                    i = i.rotate(-90)
                elif exif['Orientation'] == 3:
                    i = i.rotate(90)
            i.save(temp_dir+f'/{filenames[index]}')
            i.close()
    else:
        for index, img in enumerate([image_file1, image_file2, image_file3]):
            i = Image.open(img)
            i.save(temp_dir+f'/{filenames[index]}')
            i.close()
    face_cascade = cv2.CascadeClassifier(current_app.config['FRONTAL_FACE_CASCADE'])
    for i in os.listdir(temp_dir):
        image = cv2.imread(temp_dir+f'/{i}')
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=3, minSize=(30, 30))
        results.append(len(faces))
    #breakpoint()
    if 0 in results:
       shutil.rmtree(temp_dir)
       results = None
       response_obj['message'] = 'not a face'
       return jsonify(response_obj), 200

    # CHECK GENDER NOW BEFORE ANYTHING ELSE GETS EXECUTED
    for i in os.listdir(temp_dir):
       img_for_gender_detection = cv2.imread(temp_dir+f'/{i}')
       check_gender = gender_detector(img_for_gender_detection)
       if check_gender == 'Female':
           shutil.rmtree(temp_dir)
           response_obj['message'] = 'Only men can be added.'
           return jsonify(response_obj), 200

    duplicate = checkForMatch(current_app.config['OFFENDERS_DIR'],
    temp_dir, check_for_duplicate=True, checking_offenders_dir=True)
    #duplicate = False
    if not duplicate:
        try:
            new_offender = Offender(first_name=first_name.title(),middle_name=middle_name.title(),
                            last_name=last_name.title())
            db.session.add(new_offender)
            db.session.commit()
            new_comment = Comment(post=new_comment, owner_id=new_offender.id, author_id=userID)
            ext1 = image_filename1.split('.')[1]
            ext2 = image_filename2.split('.')[1]
            ext3 = image_filename3.split('.')[1]
            new_filename1 = token_hex(8)+'.'+ext1
            new_filename2 = token_hex(8)+'.'+ext2
            new_filename3 = token_hex(8)+'.'+ext3
            photo1 = Photo(photo_file=new_filename1, owner_id=new_offender.id)
            photo2 = Photo(photo_file=new_filename2, owner_id=new_offender.id)
            photo3 = Photo(photo_file=new_filename3, owner_id=new_offender.id)
            db.session.add_all([new_comment, photo1, photo2, photo3])
            db.session.commit()
            os.mkdir(f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}')
            os.mkdir(f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/first')
            os.mkdir(f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/second')
            os.mkdir(f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/thumbnail')
            for index, image_file in enumerate(os.listdir(temp_dir)):
                if index == 0:
                    i = Image.open(temp_dir+f'/{image_file}')
                    i.thumbnail((250, 500))
                    i.save(os.path.join(current_app.config['OFFENDERS_DIR']+f'/{new_offender.id}/first', image_file), optimize=True, quality=30)
                    i.close()
                    os.rename(f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/first/{image_file}', f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/first/{new_filename1}')
                    shutil.copyfile(f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/first/{new_filename1}', f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/second/{new_filename1}')
                    shutil.copyfile(f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/first/{new_filename1}', f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/thumbnail/{new_filename1}')
                    turn_to_thumbnail = Image.open(current_app.config['OFFENDERS_DIR']+f'/{new_offender.id}/thumbnail/{new_filename1}')
                    turn_to_thumbnail.thumbnail((500, 128))
                    turn_to_thumbnail.save(current_app.config['OFFENDERS_DIR']+f'/{new_offender.id}/thumbnail/{new_filename1}')
                    turn_to_thumbnail.close()
                else:
                    i = Image.open(temp_dir+f'/{image_file}')
                    i.thumbnail((250, 500))
                    i.save(os.path.join(current_app.config['OFFENDERS_DIR']+f'/{new_offender.id}/second', image_file), optimize=True, quality=30)
                    i.close()
                    if index == 1:
                        os.rename(f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/second/{image_file}', f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/second/{new_filename2}')
                    else:
                        os.rename(f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/second/{image_file}', f'{current_app.config["OFFENDERS_DIR"]}/{new_offender.id}/second/{new_filename3}')

            shutil.rmtree(temp_dir)
            response_obj = {
                'status': 'success',
                'message': 'offender added'
            }

            return jsonify(response_obj), 201
        except (exc.IntegrityError, ValueError):
            #print('EXCETPIOTN TRIGGERED')
            db.session.rollback()
            shutil.rmtree(temp_dir)
            return jsonify(response_obj), 500
    else:
        shutil.rmtree(temp_dir)
        response_obj['message'] = 'This man is already registered.'
        return jsonify(response_obj)


@offenders_blueprint.route('/gallery/<id>/<photo_file>', methods=['GET'])
def gallery(id, photo_file):
    target = GALLERY+f'/{id}'
    return send_from_directory(f'{target}/', photo_file, as_attachment=True)


@offenders_blueprint.route('/search', methods=['POST'])
def search_for_offender():
    response_obj = {
        'status': 'fail',
        'message': 'Invalid Payload'
    }
    image_received = request.files.get('man_to_check')
    is_smartphone = request.files.get('smartphone')

    # breakpoint()
    if image_received is None:
        name = escape(request.form.get('keyword'))
        name_coming_from_smartphone = request.get_json(silent=True)
        if name_coming_from_smartphone is not None:
            name = name_coming_from_smartphone['keyword']
        name = name.split(' ')
        if len(name) == 2:
            offenders = Offender.query.filter_by(first_name=name[0].title(), last_name=name[1].title()).limit(7).all()
        elif len(name) == 3:
            offenders = Offender.query.filter_by(first_name=name[0].title(), middle_name=name[1].title(), last_name=name[2].title()).limit(7).all()
        else:
            offenders = Offender.query.filter_by(first_name=name[0].title()).limit(7).all()
            if len(offenders) == 0:
                offenders = Offender.query.filter_by(middle_name=name[0].title()).limit(7).all()
                if len(offenders) == 0:
                    offenders = Offender.query.filter_by(last_name=name[0].title()).limit(7).all()
        if len(offenders) == 0:
            response_obj['message'] = 'no match found'
            return jsonify(response_obj), 200

        response_obj = {
            'status': 'success',
            'message': [offender.to_json() for offender in offenders]
        }
        return jsonify(response_obj), 200
    else:
        image_filename = secure_filename(image_received.filename)
        if not allowed_file(image_filename):
            response_obj['message'] = 'Only image files are allowed'
            return jsonify(response_obj)
        result = None

        temp = f'gender_check_{token_hex(8)}'
        temp_dir = current_app.config['GENDER_CHECKS_DIR']+f'/{temp}'
        os.mkdir(temp_dir)
        i = Image.open(image_received)
        i.save(temp_dir+f'/{image_filename}')
        i.close()
        # try:
            # exif = {
                # ExifTags.TAGS[k]: v
                # for k, v in i._getexif().items()
                # if k in ExifTags.TAGS
            # }
        # except AttributeError:
            # i.close()
            # temp = None
            # response_obj['message'] = 'likely fake'
            # return jsonify(response_obj), 200

        # if exif['Orientation'] != 8:
            # if exif['Orientation'] == 1:
                # i = i.rotate(180)
            # elif exif['Orientation'] == 6:
                # i = i.rotate(-90)
            # elif exif['Orientation'] == 3:
                # i = i.rotate(90)
        face_cascade = cv2.CascadeClassifier(current_app.config['FRONTAL_FACE_CASCADE'])
        image = cv2.imread(temp_dir+f'/{image_filename}')
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=3, minSize=(30, 30))
        counter = 0

        while len(faces) == 0 and counter < 4:
            i = Image.open(temp_dir+f'/{image_filename}')
            i = i.rotate(90)
            i.save(temp_dir+f'/{image_filename}')
            i.close()
            image = cv2.imread(temp_dir+f'/{image_filename}')
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=3, minSize=(30, 30))
            if len(faces) == 0:
                counter += 1

        if len(faces) == 0:
            shutil.rmtree(temp_dir)
            response_obj['message'] == 'not a face'
            return jsonify(response_obj), 200

        # NOW CHECK FOR GENDER
        img_for_gender_detection = cv2.imread(temp_dir+f'/{image_filename}')
        check_gender = gender_detector(img_for_gender_detection)

        if check_gender == 'Female':
            shutil.rmtree(temp_dir)
            response_obj['message'] = 'woman detected'
            return jsonify(response_obj), 200

        # NOW CHECK FOR MATCH
        for offender in os.listdir(current_app.config['OFFENDERS_DIR']):
            result = checkForMatch(current_app.config['OFFENDERS_DIR']+f'/{offender}/first', temp_dir, checking_offenders_dir=True, single_image_check=True)
            # print(result)
            if result:
                shutil.rmtree(temp_dir)
                match_found = Offender.query.filter_by(id=offender).first()
                comments = Comment.query.filter(Comment.owner.has(Offender.id == offender)).all()
                if is_smartphone is not None:
                    response_obj = {
                    'status': 'success',
                    'match_found': True,
                    'message': {
                        'id': match_found.id,
                        'first_name': match_found.first_name,
                        'middle_name': match_found.middle_name,
                        'last_name': match_found.last_name,
                        'photos': [photo.photo_file for photo in Photo.query.filter(Photo.owner.has(Offender.id==match_found.id)).all()]
                    },
                    'comments': [comm.to_json() for comm in comments]
                    }
                else:
                    photos = Photo.query.filter(Photo.owner.has(Offender.id == match_found.id)).all()
                    image_names = [photo.photo_file for photo in photos]
                    response_obj = {
                    'status': 'success',
                    'message': 'match found',
                    'offender_data': [match_found.to_json(), image_names],
                    'comments': [comm.to_json() for comm in comments],
                }
                #print(response_obj)
                return jsonify(response_obj), 200

        shutil.rmtree(temp_dir)
        response_obj = {
            'status': 'success',
            'message': 'no match found'
        }
        return jsonify(response_obj), 200


@offenders_blueprint.route('/fetch_profile/<id>')
def fetch_profile(id):
    profile = Offender.query.filter_by(id=id).first()
    comments = Comment.query.filter(Comment.owner.has(Offender.id == id)).all()
    response_obj = {
        'status': 'success',
        'message': {
        'id': profile.id,
        'first_name': profile.first_name,
        'middle_name': profile.middle_name,
        'last_name': profile.last_name,
        'photos': [photo.photo_file for photo in Photo.query.filter(Photo.owner.has(Offender.id==profile.id)).all()]
        },
        'comments': [comm.to_json() for comm in comments]
    }

    return jsonify(response_obj), 200


@offenders_blueprint.route('/fetch_images/<id>/<image_file>')
def fetch_images(id, image_file):
    target = current_app.config['OFFENDERS_DIR']+f'/{id}/second'
    return send_from_directory(target, image_file)


@offenders_blueprint.route('/fetch_one_image/<id>')
def fetch_one_image(id):
    try:
        target = current_app.config['OFFENDERS_DIR']+f'/{id}/thumbnail'
        filename = os.listdir(target)[0]
        return send_from_directory(target, filename)
    except FileNotFoundError:
        pass


@offenders_blueprint.route('/fetch_one_image/mobile/<id>')
def fetch_one_image_mobile(id):
    try:
        target = current_app.config['OFFENDERS_DIR']+f'/{id}/thumbnail'
        filename = os.listdir(target)[0]
        return jsonify({'status': 'success', 'message': filename})
    except FileNotFoundError:
        pass


@offenders_blueprint.route('/fetch_comments/<id>')
def fetch_comments(id):
    comments = Comment.query.filter(Comment.owner.has(Offender.id == id)).all()
    return jsonify({'status': 'success', 'message': [comment.to_json() for comment in comments]})
