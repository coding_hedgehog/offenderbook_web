from project import create_app, db
from project.api.models import Comment, Offender, Photo
import time, shutil, subprocess

subprocess.run('sudo chmod 777 -R project/gallery/offenders', shell=True)


def delete_unconfirmed_comments():
    app = create_app()
    app.app_context().push()
    comments = Comment.query.all()

    if len(comments) != 0:
        for comment in comments:
            comm_timestamp = comment.created.timestamp()
            # if time.time() > comm_timestamp + 300 and not comment.confirmed:
            #     Comment.query.filter_by(id=comment.id).delete()
            #     db.session.commit()

            # UNCOMMENT THE BELOW IN PRODUCTION
            if time.time() > comm_timestamp + 86400 and not comment.confirmed:
               Comment.query.filter_by(id=comment.id).delete()
               db.session.commit()

    offenders = Offender.query.all()
    if len(offenders) != 0:
        for offender in offenders:
            comments = Comment.query.filter(Comment.owner.has(Offender.id == offender.id)).all()
            #print(len(comments))
            if len(comments) == 0:
                photos = Photo.query.filter(Photo.owner.has(Offender.id == offender.id)).all()
                for photo in photos:
                    Photo.query.filter_by(id=photo.id).delete()
                Offender.query.filter_by(id=offender.id).delete()
                shutil.rmtree(f'project/gallery/offenders/{offender.id}')
                db.session.commit()

    return 0


if __name__ == '__main__':
    delete_unconfirmed_comments()
