from project import create_app, db
from project.api.models import Comment, Offender, Photo
import time, shutil, sys

app = create_app()
app.app_context().push()
offenders = Offender.query.all()

def delete_offender_after_year_of_no_activity():
    if len(offenders) != 0:
        for offender in offenders:
            his_warnings = Comment.query.filter(Comment.owner.has(Offender.id == offender.id)).all()
            latest_warning = his_warnings[-1]
            created = latest_warning.created
            timestamp = created.timestamp()
            # if time.time() > timestamp + 600:
            #     photos = Photo.query.filter(Photo.owner.has(Offender.id == offender.id)).all()
            #     comments = Comment.query.filter(Comment.owner.has(Offender.id == offender.id)).all()
            #     for photo in photos:
            #         Photo.query.filter_by(id=photo.id).delete()
            #     for comment in comments:
            #         Comment.query.filter_by(id=comment.id).delete()
            #     Offender.query.filter_by(id=offender.id).delete()
            #     shutil.rmtree(f'project/gallery/offenders/{offender.id}')
            #     db.session.commit()
            # UNCOMMENT THE BELOW IN PRODUCTION
            if time.time() > timestamp + 63072000:
                photos = Photo.query.filter(Photo.owner.has(Offender.id == offender.id)).all()
                comments = Comment.query.filter(Comment.owner.has(Offender.id == offender.id)).all()
                for photo in photos:
                    Photo.query.filter_by(id=photo.id).delete()
                for comment in comments:
                    Comment.query.filter_by(id=comment.id).delete()
                Offender.query.filter_by(id=offender.id).delete()
                shutil.rmtree(f'project/gallery/offenders/{offender.id}')
                db.session.commit()
        return 0
    else:
        sys.exit(0)

if __name__ == '__main__':
    delete_offender_after_year_of_no_activity()
