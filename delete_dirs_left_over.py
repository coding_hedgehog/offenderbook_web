import os, time, shutil, subprocess

subprocess.run('sudo chmod 777 -R project/passport_checks', shell=True)
subprocess.run('sudo chmod 777 -R project/gallery/orientation_checker', shell=True)


for dir in os.listdir('/home/mark/offenderbook/project/gallery/orientation_checker/'):
    if int(time.ctime(os.path.getctime('/home/mark/offenderbook/project/gallery/orientation_checker'+f'/{dir}'))[14:16]) + 15 >= time.gmtime()[4]:
        shutil.rmtree('/home/mark/offenderbook/project/gallery/orientation_checker'+f'/{dir}')

for dir in os.listdir('/home/mark/offenderbook/project/passport_checks'):
    if int(time.ctime(os.path.getctime('/home/mark/offenderbook/project/passport_checks'+f'/{dir}'))[14:16]) + 15 >= time.gmtime()[4]:
        shutil.rmtree('/home/mark/offenderbook/project/passport_checks'+f'/{dir}')
