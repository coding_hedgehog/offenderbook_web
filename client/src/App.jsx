import React from "react";
import { Switch, Route } from "react-router-dom";
import axios from "axios";
import Main from "./components/Main";
import Footer from "./components/Footer";


function App() {
  return (
    <>
      <Main />
      <Footer />
    </>
  );
}

export default App;
