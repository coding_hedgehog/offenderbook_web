import React, { useState, useEffect } from "react";
import { useGlobalContext } from "./context";
import { CgAsterisk } from "react-icons/cg";
import { FaUserAlt } from "react-icons/fa";
import { BsFillLockFill } from "react-icons/bs";
import Navbar from "./Navbar";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";

const Login = () => {
  const {
    sessionUsername,
    setSessionUsername,
    setSessionUsernameID,
    justLoggedOutNotif,
    setJustLoggedOutNotif,
    setJustLoggedIn,
    setTrace,
    setResetSwitch,
    resetSwitch
  } = useGlobalContext();
  const [username_or_email, setUsernameOrEmail] = useState("");
  const [displayWentWrongWarning, setWentWrongWarning] = useState(false);
  const [displayConfirmEmailNot, setConfirmEmailNot] = useState(false);
  const [loginBtnIsDisabled, setLoginBtnDisabled] = useState(true);
  const [displayLoginFailed, setLoginFailed] = useState(false);
  const [password, setPassword] = useState("");

  const passwordStylingOn = { fontFamily: "Orbitron" };
  const passwordStylingOff = { fontFamily: "Verdana" };

  const handleSubmit = async e => {
    e.preventDefault();

    const url = "/auth/login";
    const headers = { "Access-Control-Allow-Origin": "*" };
    const data = {
      username_or_email: username_or_email,
      password: password
    };
    try {
      const resp = await axios.post(url, data, headers);

      if (resp.data.message === "This user does not exist.") {
        setLoginFailed(true);
      } else if (resp.data.message === "Successfully logged in.") {
        setSessionUsername(resp.data.username);
        setSessionUsernameID(resp.data.user_id);
        setJustLoggedIn(true);
        setUsernameOrEmail("");
        setPassword("");
        localStorage.setItem("user", JSON.stringify(resp.data));
      } else if (resp.data.message === "confirm email first") {
        setConfirmEmailNot(true);
      }
    } catch (err) {
      setWentWrongWarning(true);
    }
  };

  useEffect(() => {
    setTrace("/login");
    document.getElementsByTagName(
      "body"
    )[0].style = `height: ${window.screen.height}px`;
    document.getElementsByTagName('title')[0].innerHTML = 'Log Into Offenderbook';
    document
      .querySelector('meta[name="description"]')
      .setAttribute("content", "User Login");
  }, []);

  useEffect(() => {
    if (username_or_email && password) {
      setLoginBtnDisabled(false);
    } else {
      setLoginBtnDisabled(true);
    }
  });

  useEffect(() => {
    if (displayWentWrongWarning) {
      const timeout = setTimeout(() => {
        setWentWrongWarning(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [displayWentWrongWarning]);

  useEffect(() => {
    if (displayConfirmEmailNot) {
      const timeout = setTimeout(() => {
        setConfirmEmailNot(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [displayConfirmEmailNot]);

  useEffect(() => {
    if (displayConfirmEmailNot) {
      const timeout = setTimeout(() => {
        setWentWrongWarning(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [displayConfirmEmailNot]);

  useEffect(() => {
    if (displayLoginFailed) {
      const timeout = setTimeout(() => {
        setLoginFailed(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [displayLoginFailed]);

  useEffect(() => {
    setResetSwitch(false);
  }, [resetSwitch]);

  useEffect(() => {
    if (justLoggedOutNotif) {
      const timeout = setTimeout(() => {
        setJustLoggedOutNotif(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [justLoggedOutNotif]);

  return (
    <>
      {sessionUsername && <Redirect to="/" />}
      <Navbar />
      <div className="columns">
        <div className="column is-one-third"></div>
        <div className="column is-one-third">
          <form onSubmit={handleSubmit} id="login_form">
            {displayLoginFailed && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  Login Failed. Please Double-Check Your Details.
                </p>
              </div>
            )}
            {displayConfirmEmailNot && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  Please Confirm Your Email First.
                </p>
              </div>
            )}
            {justLoggedOutNotif && (
              <div className="notification is-info">
                <p style={{ color: "white" }}>
                  You're now logged out. See you soon !
                </p>
              </div>
            )}
            {displayWentWrongWarning && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  Sorry, something went wrong. Try again later or if problem
                  persists get in touch with us through contact form or social
                  media.
                </p>
              </div>
            )}
            <h5 className="title is-5" style={{ color: "white" }}>
              Login
            </h5>
            <div className="field">
              <div className="control has-icons-left has-icons-right">
                <input
                  onChange={e => setUsernameOrEmail(e.target.value)}
                  className="input is-rounded is-focused"
                  type="text"
                  placeholder="username or @"
                  value={username_or_email}
                  autoFocus="autofocus"
                />
                <span className="icon is-small is-left">
                  <FaUserAlt />
                </span>
                <span className="icon is-small is-right">
                  <CgAsterisk />
                </span>
              </div>
            </div>

            <div className="field">
              <div className="control has-icons-left has-icons-right">
                <input
                  onChange={e => setPassword(e.target.value)}
                  style={
                    password.length === 0
                      ? passwordStylingOn
                      : passwordStylingOff
                  }
                  className="input is-rounded"
                  type="password"
                  placeholder="Password"
                  value={password}
                  id="required"
                />
                <span className="icon is-small is-left">
                  <BsFillLockFill />
                </span>
                <span className="icon is-small is-right">
                  <CgAsterisk />
                </span>
              </div>
            </div>

            <div className="field is-grouped">
              <div className="control">
                {!loginBtnIsDisabled && (
                  <button className="button is-rounded is-outline">
                    Login
                  </button>
                )}
                {loginBtnIsDisabled && (
                  <button className="button is-rounded is-outline" disabled>
                    Login
                  </button>
                )}
              </div>
              <div className="control">
                <Link
                  to="/"
                  id="cancel_link"
                  className="button is-rounded is-outline"
                >
                  Cancel
                </Link>
              </div>
            </div>
          </form>
        </div>
        <div className="column is-one-third"></div>
      </div>
    </>
  );
};

export default Login;
