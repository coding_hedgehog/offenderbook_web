import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useGlobalContext } from "./context";
import Logo from "../assets/navbar_logo.png";
import { MdPersonAdd } from "react-icons/md";
import { FaComment, FaInstagram, FaTwitter, FaMastodon } from "react-icons/fa";
import { FcSearch } from "react-icons/fc";
import { BsUpload } from "react-icons/bs";
import axios from "axios";

const Navbar = props => {
  const {
    sessionUsername,
    setSessionUsername,
    setSessionUsernameID,
    setJustLoggedOutNotif,
    setSearchedKeyword,
    searchedKeyword,
    resetFileFieldsJustRan,
    setResetJustRan,
    trace
  } = useGlobalContext();
  const [displayLogoInNavbar, setLogoInNavbar] = useState(false);
  const [randomNum, setRandomNum] = useState(Math.random());
  const {
    displaySearchBox,
    keyword,
    setKeyword,
    onSearch,
    onAddOffender,
    onHandleSwitch,
    displayLogo,
    isChecking,
    setFile
  } = props;

  const handleLogout = () => {
    const timeout = setTimeout(() => {
      setSessionUsername("");
      setSessionUsernameID("");
      localStorage.clear();
      setJustLoggedOutNotif(true);
    }, 2000);
    return () => clearTimeout(timeout);
  };

  useEffect(() => {
    if (
      window.location.href.includes("login") ||
      window.location.href.includes("join") ||
      displayLogo
    ) {
      setLogoInNavbar(true);
    }
  }, [displayLogo]);

  useEffect(() => {
    if (searchedKeyword !== keyword) {
      setSearchedKeyword(keyword);
    }
  }, [keyword]);

  useEffect(() => {
    let width = window.screen.width / 2 - 400;
    if (displaySearchBox && sessionUsername) {
      document.getElementsByTagName(
        "form"
      )[0].style = `margin-left: ${width}px;margin-top: 7px`;
    } else if (displaySearchBox && !sessionUsername) {
      document.getElementsByTagName("form")[0].style = `margin-left: ${width -
        80}px;margin-top: 7px`;
    }
  }, [displaySearchBox]);

  return (
    <>
      <nav
        className="navbar is-transparent"
        role="navigation"
        aria-label="main navigation"
      >
        {displayLogoInNavbar && (
          <>
            <div className="navbar-brand" style={{ width: "250px" }}>
              <img
                style={{
                  width: "auto",
                  height: "40%",
                  marginTop: "5px",
                  marginLeft: "5px",
                  marginBottom: "0"
                }}
                src={Logo}
                alt="offenderbook logo"
              />
            </div>
          </>
        )}

        {!displayLogoInNavbar && (
          <>
            <div className="navbar-brand">
              {sessionUsername && (
                <p
                  style={{
                    color: "white",
                    marginTop: "10px",
                    marginLeft: "10px"
                  }}
                >
                  Hi, {sessionUsername} !
                </p>
              )}
            </div>
          </>
        )}

        {displaySearchBox && (
          <>
            <form onSubmit={onSearch} className="form">
              <div className="field">
                <div className="control has-icons-right">
                  <input
                    onChange={e => setKeyword(e.target.value)}
                    className="input is-medium is-rounded"
                    type="text"
                    placeholder="i.e. John Doe"
                    value={keyword}
                    autoFocus="autofocus"
                  />

                  <span className="icon is-small is-right">
                    <FcSearch />
                  </span>
                </div>
              </div>
            </form>

            {sessionUsername && (
              <button
                type="button"
                style={{ marginLeft: "10px", marginTop: "7px" }}
                className="button is-rounded is-outlined"
                onClick={() => onHandleSwitch()}
              >
                <MdPersonAdd />
              </button>
            )}

            {!isChecking && (
              <>
                <div
                  className="file is-white"
                  style={{ marginLeft: "10px", marginTop: "7px" }}
                >
                  <label className="file-label">
                    <input
                      className="file-input"
                      type="file"
                      name="upload"
                      onChange={e => setFile(e.target.files[0])}
                    />
                    <span className="file-cta">
                      <span className="file-icon">
                        <BsUpload />
                      </span>
                    </span>
                  </label>
                </div>
              </>
            )}
            {isChecking && (
              <button
                style={{ marginLeft: "10px", marginTop: "7px" }}
                className="button is-loading"
              >
                Loading
              </button>
            )}
          </>
        )}

        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start"></div>
        </div>

        <div className="navbar-end">
          <a href="https://www.instagram.com/offenderbook" target="_blank">
            <FaInstagram size={25} style={{ marginTop: "7px" }} />
          </a>
          &nbsp;&nbsp;
          <a href="https://twitter.com/offenderbook1" target="_blank">
            <FaTwitter size={25} style={{ marginTop: "7px" }} />
          </a>
          &nbsp;&nbsp;
          <a href="https://mastodon.online/@offenderbook" target="_blank">
            <FaMastodon size={25} style={{ marginTop: "7px" }} />
          </a>
          {!displayLogoInNavbar && (
            <div className="navbar-item">
              {trace !== "/feedback" && (
                <div
                  style={{
                    cursor: "pointer",
                    color: "white",
                    marginRight: "20px"
                  }}
                >
                  <Link to="/feedback">
                    <FaComment size={25} />
                  </Link>
                </div>
              )}
              <div className="buttons">
		<a target="_blank" href="http://blog.offenderbook.com" alt="link to blog">Blog</a>
                <Link id="about_link" to="/about" alt="link to about page">
                  About
                </Link>
                {!sessionUsername && (
                  <Link id="join_link" to="/join" alt="link to join page">
                    Join
                  </Link>
                )}
                {!sessionUsername && (
                  <Link id="login_link" to="/login" alt="link to login page">
                    Login
                  </Link>
                )}
                {sessionUsername && (
                  <Link
                    id="logout_link"
                    alt="link to logout"
                    to="#"
                    onClick={() => handleLogout()}
                  >
                    Logout
                  </Link>
                )}
              </div>
            </div>
          )}
          {displayLogoInNavbar && (
            <div
              className="navbar-item"
              style={{ display: "flex !important", marginTop: "0" }}
            >
              {trace !== "/feedback" && (
                <div
                  style={{
                    cursor: "pointer",
                    color: "white",
                    marginRight: "20px"
                  }}
                >
                  <Link to="/feedback">
                    <FaComment size={30} />
                  </Link>
                </div>
              )}

              <div className="buttons">
                {trace !== "/about" && (
                  <Link id="about_link" to="/about" alt="link to about page">
                    About
                  </Link>
                )}

                {!sessionUsername && trace !== "/join" && (
                  <Link id="join_link" to="/join" alt="link to join page">
                    Join
                  </Link>
                )}
                {!sessionUsername && trace !== "/login" && (
                  <Link id="login_link" to="/login" alt="link to login page">
                    Login
                  </Link>
                )}
                {sessionUsername && (
                  <Link
                    id="logout_link"
                    alt="link to logout"
                    to="#"
                    onClick={() => handleLogout()}
                  >
                    Logout
                  </Link>
                )}
              </div>
            </div>
          )}
        </div>
      </nav>
    </>
  );
};

export default Navbar;
