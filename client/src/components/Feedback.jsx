import React, { useState, useEffect } from "react";
import { useGlobalContext } from "./context";
import { Link } from "react-router-dom";
import Pic1 from "../assets/captcha/pic1.png";
import Pic2 from "../assets/captcha/pic2.png";
import Pic3 from "../assets/captcha/pic3.png";
import Pic4 from "../assets/captcha/pic4.png";
import Pic5 from "../assets/captcha/pic5.png";
import Pic6 from "../assets/captcha/pic6.png";
import Pic7 from "../assets/captcha/pic7.png";
import Navbar from "./Navbar";
import axios from "axios";

const Feedback = () => {
  const { trace, setTrace, sessionUsername } = useGlobalContext();
  const [feedback, setFeedback] = useState("");
  const [profanityTrigger, setProfanityTrigger] = useState(false);
  const [displaySuccessNot, setDisplaySuccessNot] = useState(false);
  const [displayFailureNot, setDisplayFailureNot] = useState(false);
  const [captchaFailed, setCaptchaFailed] = useState(false);
  const pictures = [Pic1, Pic2, Pic3, Pic4, Pic5, Pic6, Pic7];
  const results = [5, 10, 7, 1, 4, 0, 2];
  const [pickedCaptcha, setCaptcha] = useState();
  const [pickedIndex, setIndex] = useState();
  const [sendBtnDisabled, setSendBtnDisabled] = useState(true);
  const [
    displayCharacterLengthWarning,
    setDisplayCharacterLengthWarning
  ] = useState(false);
  const [result, setResult] = useState("");

  const handleSubmit = async e => {
    e.preventDefault();
    if (feedback.length < 25) {
      setDisplayCharacterLengthWarning(true);
    } else if (
      feedback.includes("fuck") ||
      feedback.includes("suck") ||
      feedback.includes("f**k") ||
      feedback.includes("s**k") ||
      feedback.includes("shit") ||
      feedback.includes("s**t") ||
      feedback.includes("sh*t") ||
      feedback.includes("fu*k") ||
      feedback.includes("ass") ||
      feedback.includes("asshole") ||
      feedback.includes("a**hole") ||
      feedback.includes("jerk")
    ) {
      setProfanityTrigger(true);
    } else {
      if (parseInt(result) === results[pickedIndex]) {
        const fd = new FormData();
        const url = "/auth/submit_feedback";
        fd.append("feedback", feedback);
        try {
          const resp = await axios.post(url, fd);
          if (resp) {
            if (resp.data.status === "success") {
              setDisplaySuccessNot(true);
              setFeedback("");
              setResult("");
            } else {
              setDisplayFailureNot(true);
            }
          }
        } catch (e) {
          console.log(e);
          setDisplayFailureNot(true);
        }
      } else {
        setCaptchaFailed(true);
        setCaptcha(getRandomPic(pictures));
      }
    }
  };

  const getRandomPic = arr => {
    let index = String(Math.random())[2];
    while (parseInt(index) > 6) {
      index = String(Math.random())[2];
    }
    setIndex(parseInt(index));
    return arr[index];
  };

  useEffect(() => {
    if (!sessionUsername) {
      if (feedback.length !== 0 && result.length !== 0) {
        setSendBtnDisabled(false);
      } else {
        setSendBtnDisabled(true);
      }
    } else {
      if (feedback.length !== 0) {
        setSendBtnDisabled(false);
      } else {
        setSendBtnDisabled(true);
      }
    }
  });

  useEffect(() => {
    setCaptcha(getRandomPic(pictures));
    document.getElementsByTagName(
      "body"
    )[0].style = `height: ${window.screen.height}px`;
    document
      .querySelector('meta[name="description"]')
      .setAttribute("content", "Send Us Feedback");
  }, []);

  useEffect(() => {
    if (profanityTrigger) {
      const timeout = setTimeout(() => {
        setProfanityTrigger(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [profanityTrigger]);

  useEffect(() => {
    if (displayFailureNot) {
      const timeout = setTimeout(() => {
        setDisplayFailureNot(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [displayFailureNot]);

  useEffect(() => {
    if (captchaFailed) {
      const timeout = setTimeout(() => {
        setCaptchaFailed(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [captchaFailed]);

  useEffect(() => {
    if (displaySuccessNot) {
      const timeout = setTimeout(() => {
        setDisplaySuccessNot(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [displaySuccessNot]);

  useEffect(() => {
    if (displayCharacterLengthWarning) {
      const timeout = setTimeout(() => {
        setDisplayCharacterLengthWarning(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [displayCharacterLengthWarning]);

  useEffect(() => {
    setTrace("/feedback");
  });

  return (
    <>
      <Navbar displayLogo={"true"} />
      <div className="columns" style={{ marginTop: "40px" }}>
        <div className="column is-one-third"></div>
        <div className="column is-one-third">
          {displayCharacterLengthWarning && (
            <div
              className="notification is-warning"
              style={{
                marginTop: "20px"
              }}
            >
              <p style={{ color: "#cc3300", fontWeight: "bold" }}>
                Your Feedback Needs To Be At Least 25 Characters Long.
              </p>
            </div>
          )}
          {displayFailureNot && (
            <div
              className="notification is-warning"
              style={{
                marginTop: "20px"
              }}
            >
              <p style={{ color: "#cc3300", fontWeight: "bold" }}>
                Sorry Something Went Wrong. If Problem Persist Reach Out To Us
                On Twitter Or Mastodon.
              </p>
            </div>
          )}
          {profanityTrigger && (
            <div
              className="notification is-warning"
              style={{
                marginTop: "20px"
              }}
            >
              <p style={{ color: "#cc3300", fontWeight: "bold" }}>
                No Swear Words Are Allowed. Let's Keep Things Clean :)
              </p>
            </div>
          )}
          {captchaFailed && (
            <div
              className="notification is-warning"
              style={{
                marginTop: "20px"
              }}
            >
              <p style={{ color: "#cc3300", fontWeight: "bold" }}>
                Human Check Failed. Please Try Again :)
              </p>
            </div>
          )}
          {displaySuccessNot && (
            <div className="notification is-success">
              <p style={{ color: "white" }}>Feedback Sent. Thank You !</p>
            </div>
          )}
          <form onSubmit={handleSubmit}>
            <h5 className="title is-5" style={{ color: "white" }}>
              How Can Offenderbook Be Better ?
            </h5>
            <div className="field">
              <div className="control">
                <textarea
                  onChange={e => setFeedback(e.target.value)}
                  className="input is-focused"
                  name="feedback"
                  rows="4"
                  style={{ height: "100px" }}
                  placeholder="I'd like to see ..., I think this could be improved, ..."
                  value={feedback}
                  autoFocus="autofocus"
                />
              </div>
            </div>
            {!sessionUsername && (
              <>
                <div style={{ display: "inline-flex" }}>
                  <img src={pickedCaptcha} alt="captcha challenge" />
                  <div className="field">
                    <div className="control">
                      <input
                        onChange={e => setResult(e.target.value)}
                        style={{
                          width: "40%",
                          marginLeft: "20px",
                          marginTop: "20px"
                        }}
                        className="input is-rounded is-focused"
                        name="result"
                        type="number"
                        value={result}
                      />
                    </div>
                  </div>
                </div>
              </>
            )}

            {sendBtnDisabled && (
              <button className="button is-rounded is-outline" disabled>
                Send
              </button>
            )}
            {!sendBtnDisabled && (
              <button className="button is-rounded is-outline">Send</button>
            )}
          </form>
        </div>
        <div className="column is-one-third">
          <Link
            style={{ marginLeft: "30px" }}
            to="/"
            className="delete is-large"
          ></Link>
        </div>
      </div>
    </>
  );
};

export default Feedback;
