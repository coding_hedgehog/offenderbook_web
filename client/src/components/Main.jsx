import React, { useState, useEffect } from "react";
import Navbar from "./Navbar";
import Logo from "../assets/offenderbook.png";
import { useGlobalContext } from "./context";
import { Link, Redirect } from "react-router-dom";
import AddOffenderForm from "./AddOffenderForm";
import { FcSearch } from "react-icons/fc";
import { GiAerialSignal } from "react-icons/gi";
import { BsExclamationTriangle, BsUpload, BsSearch } from "react-icons/bs";
import { MdPersonAdd } from "react-icons/md";
import Typing from "../assets/typing.png";
import Arrow from "../assets/arrow.png";
import Profile from "./Profile";
import axios from "axios";

const Main = () => {
  const {
    justLoggedOutNotif,
    setJustLoggedOutNotif,
    setResetSwitch,
    sessionUsername,
    setSessionUsername,
    setSearchedKeyword,
    setSessionUsernameID,
    sessionUsernameID,
    searchedKeyword,
    justLoggedIn,
    setJustLoggedIn,
    setRefreshTrigger,
    resetSwitch,
    setTrace,
    trace
  } = useGlobalContext();
  const [displayAddOffenderForm, setAddOffenderForm] = useState(false);
  const [displayFemaleWarning, setDisplayFemaleWarning] = useState(false);
  const [invalidFileExtension, setInvalidFileExtension] = useState(false);
  const [displayFoundMatchNot, setDisplayFoundMatchNot] = useState(false);
  const [displayNoMatchFoundNot, setDisplayNoMatchFoundNot] = useState(false);
  const [displayHint, setDisplayHint] = useState(false);
  const [isConsent, setConsent] = useState(false);
  const [displayKeywordMatch, setDisplayKeywordMatch] = useState(false);
  const [displayMatch, setDisplayMatch] = useState(false);
  const [displayProfile, setDisplayProfile] = useState(false);
  const [checking, setChecking] = useState(false);
  const [emptySearchWarning, setEmptySearchWarning] = useState(false);
  const [displayLogo, setDisplayLogo] = useState(false);
  const [isPhotoSearch, setIsPhotoSearch] = useState(false);
  const [matchFound, setMatchFound] = useState({});
  const [comments, setComments] = useState([]);
  const [keywordMatches, setKeywordMatches] = useState([]);
  const [fileToCheck, setFile] = useState();
  const [firstName, setFirstName] = useState("");
  const [middleName, setMiddleName] = useState("");
  const [lastName, setLastName] = useState("");
  const [keyword, setKeyword] = useState("");
  const [firstPhoto, setFirstPhoto] = useState("");
  const [secondPhoto, setSecondPhoto] = useState("");
  const [thirdPhoto, setThirdPhoto] = useState("");
  const [selectedProfileID, setSelectedProfileID] = useState("");
  const [isMobile, setIsMobile] = useState(
    window.screen.width < 1000 ? true : false
  );

  const handleSubmit = async e => {
    e.preventDefault();
    if (keyword === "") {
      setEmptySearchWarning(true);
    } else {
      const url = "/offenders/search";
      const headers = {
        "Content-Type": "multipart/form-data"
      };
      const fd = new FormData();
      fd.append("keyword", keyword);
      try {
        const resp = await axios.post(url, fd, headers);
        if (resp.data.status === "success") {
          setKeywordMatches(resp.data.message);
          setDisplayHint(false);
          if (!sessionUsername) {
            setDisplayLogo(true);
          }

          setDisplayKeywordMatch(true);
          setSearchedKeyword(keyword);
          let str = document.getElementById("name_to_check").innerHTML;
          if (str.includes("&amp;#39;")) {
            let res = str.replace("&amp;#39;", "'");
            document.getElementById("name_to_check").innerHTML = res;
          }
        } else {
          setDisplayNoMatchFoundNot(true);
          setDisplayHint(true);
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  const handleSwitchToAddOffenderForm = () => {
    setDisplayKeywordMatch(false);
    setAddOffenderForm(true);
    setKeyword("");
  };

  const handleSecondSwitchToAddOffenderForm = () => {
    setAddOffenderForm(true);
    setKeyword("");
  };

  const handleSwitchingBack = () => {
    setComments([]);
    setMatchFound({});
    setDisplayMatch(false);
    if (!sessionUsername && displayKeywordMatch) {
      setDisplayLogo(true);
    }

    setFirstPhoto("");
    setSecondPhoto("");
    setThirdPhoto("");
    if (isPhotoSearch) {
      setDisplayProfile(false);
      setKeywordMatches(false);
      setIsPhotoSearch(false);
      setKeyword("");
      document.getElementsByTagName(
        "body"
      )[0].style = `height: ${window.screen.height}px`;
    } else {
      const url = "/offenders/search";
      const headers = {
        "Content-Type": "multipart/form-data"
      };
      const fd = new FormData();
      fd.append("keyword", searchedKeyword);
      try {
        axios.post(url, fd, headers).then(resp => {
          if (resp.data.status === "success") {
            setKeywordMatches(resp.data.message);
            setDisplayHint(false);
            setSearchedKeyword(searchedKeyword);
            setDisplayProfile(false);
            setDisplayKeywordMatch(true);
            if (keywordMatches.length > 5) {
              document.getElementsByTagName("body")[0].style = `height: ${window
                .screen.height * 3}px`;
            } else {
              document.getElementsByTagName(
                "body"
              )[0].style = `height: ${window.screen.height}px`;
            }
          } else {
            setDisplayNoMatchFoundNot(true);
            setDisplayHint(true);
          }
        });
      } catch (e) {
        console.log(e);
      }
    }

    window.scrollTo(0, 0);
  };

  const handlePhotoSearch = async e => {
    setChecking(true);
    const fd = new FormData();
    fd.append("man_to_check", fileToCheck, fileToCheck.name);
    const url = "/offenders/search";
    const headers = {
      "Content-Type": "multipart/form-data"
    };

    try {
      const resp = await axios.post(url, fd, headers);
      if (resp) {
        setChecking(false);
        if (resp.data.message === "Only image files are allowed") {
          setInvalidFileExtension(true);
        } else if (resp.data.message === "match found") {
          setMatchFound(resp.data.offender_data[0]);
          setFirstPhoto(
            `/offenders/fetch_images/${resp.data.offender_data[0].id}/${resp.data.offender_data[1][0]}`
          );
          setSecondPhoto(
            `/offenders/fetch_images/${resp.data.offender_data[0].id}/${resp.data.offender_data[1][1]}`
          );
          setThirdPhoto(
            `/offenders/fetch_images/${resp.data.offender_data[0].id}/${resp.data.offender_data[1][2]}`
          );
          const obj = resp.data.comments;
          setComments(obj);
          setDisplayKeywordMatch(false);
          setDisplayFoundMatchNot(true);
          setIsPhotoSearch(true);
          setDisplayMatch(true);
        } else if (resp.data.message === "no match found") {
          setDisplayNoMatchFoundNot(true);
        } else if (resp.data.message === "Only men can be added.") {
          setDisplayFemaleWarning(true);
        } else {
          //console.log(resp.data.message);
        }
      }
    } catch (e) {
      console.log(e);
    }
    setFile("");
  };

  const handleConsent = () => {
    const data = {
      consent: "given"
    };
    localStorage.setItem("offenderbook_consent", JSON.stringify(data));
    setConsent(true);
    document.getElementById("cookie_bar").style = "display: none";
  };

  const displayProfileID = async id => {
    const url = `/offenders/fetch_profile/${id}`;

    try {
      const resp = await axios.get(url);
      if (resp.data.status === "success") {
        setFirstName(resp.data.message.first_name);
        setMiddleName(resp.data.message.middle_name);
        setLastName(resp.data.message.last_name);

        setFirstPhoto(
          `/offenders/fetch_images/${id}/${resp.data.message.photos[0]}`
        );
        setSecondPhoto(
          `/offenders/fetch_images/${id}/${resp.data.message.photos[1]}`
        );
        setThirdPhoto(
          `/offenders/fetch_images/${id}/${resp.data.message.photos[2]}`
        );

        setComments(resp.data.comments);
        setSelectedProfileID(id);
        setDisplayKeywordMatch(false);
        setDisplayProfile(false);
        if (displayMatch) {
          setDisplayMatch(false);
        }
      }
    } catch (e) {
      console.log(e);
    }
    if (!displayMatch && !isPhotoSearch) {
      setDisplayProfile(true);
    } else {
      setDisplayMatch(true);
    }
  };

  useEffect(() => {
    document.getElementsByTagName(
      "body"
    )[0].style = `height: ${window.screen.height}px`;
    document.getElementsByTagName("title")[0].innerHTML = "Offenderbook - Making Dating Safer";
    document.getElementsByTagName(
      "footer"
    )[0].style = `position:absolute;top: ${window.screen.height}px;width:100%;background-color:#f63c58;padding: 10px 24px`;
    setTrace("/");
  }, []);

  useEffect(() => {
    if (!sessionUsername) {
      let user = localStorage.getItem("user");
      if (user) {
        let foundUser = JSON.parse(user);
        setSessionUsername(foundUser.username);
        setSessionUsernameID(foundUser.user_id);
      }
    }
    if (fileToCheck) {
      handlePhotoSearch();
    }
  }, [fileToCheck]);

  useEffect(() => {
    if (resetSwitch) {
      setResetSwitch(false);
    }
  }, [resetSwitch]);

  useEffect(() => {
    if (
      displayProfile ||
      displayMatch ||
      (displayKeywordMatch && !sessionUsername)
    ) {
      if (keywordMatches.length > 5) {
        console.log("FIRED *2");
        document.getElementsByTagName("body")[0].style = `height: ${window
          .screen.height * 2}px`;
      } else {
        document.getElementsByTagName(
          "body"
        )[0].style = `height: ${window.screen.height}px`;
      }
      setDisplayLogo(true);
    }
  }, [!sessionUsername]);

  useEffect(() => {
    if (displayMatch) {
      document.getElementsByTagName("body")[0].style = "height: auto";
    }
  }, [displayMatch]);

  useEffect(() => {
    if (keyword === "") {
      setDisplayHint(true);
      setKeywordMatches([]);
    }
  }, [keyword]);

  useEffect(() => {
    if (invalidFileExtension) {
      const timeout = setTimeout(() => {
        setInvalidFileExtension(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [invalidFileExtension]);

  useEffect(() => {
    if (displayFoundMatchNot) {
      const timeout = setTimeout(() => {
        setDisplayFoundMatchNot(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [displayFoundMatchNot]);

  useEffect(() => {
    if (displayKeywordMatch) {
      if (keywordMatches.length > 5) {
        console.log("FIRED *2");
        document.getElementsByTagName("body")[0].style = `height: ${window
          .screen.height * 3}px`;
      } else {
        document.getElementsByTagName(
          "body"
        )[0].style = `height: ${window.screen.height}px`;
      }
      let str = document.getElementById("name_to_check").innerHTML;
      if (str.includes("&amp;#39;")) {
        let res = str.replace("&amp;#39;", "'");
        document.getElementById("name_to_check").innerHTML = res;
      }
    }
  }, [displayKeywordMatch]);

  useEffect(() => {
    if (emptySearchWarning) {
      const timeout = setTimeout(() => {
        setEmptySearchWarning(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [emptySearchWarning]);

  useEffect(() => {
    if (displayNoMatchFoundNot) {
      setKeywordMatches([]);
      const timeout = setTimeout(() => {
        setDisplayNoMatchFoundNot(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [displayNoMatchFoundNot]);

  useEffect(() => {
    if (displayFemaleWarning) {
      const timeout = setTimeout(() => {
        setDisplayFemaleWarning(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [displayFemaleWarning]);

  useEffect(() => {
    if (justLoggedIn) {
      const timeout = setTimeout(() => {
        setJustLoggedIn(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [justLoggedIn]);

  useEffect(() => {
    if (!isConsent) {
      let consent = localStorage.getItem("offenderbook_consent");
      if (!consent) {
        document.getElementsByTagName("footer")[0].style = "display: none";
      } else {
        setConsent(true);
        document.getElementById("cookie_bar").style = "display: none";
        document.getElementsByTagName("footer")[0].style = "display: block";
      }
    } else {
      document.getElementsByTagName(
        "footer"
      )[0].style = `position:absolute;top: ${window.screen.height}px;width:100%;background-color:#f63c58;padding: 10px 24px`;
    }
  }, [isConsent]);

  return (
    <>
      {!window.location.href.includes("confirm_email_mobile") && isMobile && (
        <Redirect to="/computers_only" />
      )}
      {justLoggedOutNotif && <Redirect to="/login" />}
      <Navbar
        displaySearchBox={displayKeywordMatch}
        setKeyword={setKeyword}
        keyword={keyword}
        onSearch={handleSubmit}
        onHandleSwitch={handleSwitchToAddOffenderForm}
        setFile={setFile}
        isChecking={checking}
        displayLogo={displayLogo}
      />
      {!displayMatch && !displayKeywordMatch && !displayProfile && (
        <>
          {!displayAddOffenderForm && (
            <>
              <div className="columns" style={{ marginTop: "50px" }}>
                <div className="column is-one-third"></div>
                <div className="column is-one-third">
                  {justLoggedIn && (
                    <>
                      <div className="notification is-success">
                        <p style={{ color: "white" }}>
                          Welcome, {sessionUsername} !
                        </p>
                      </div>
                    </>
                  )}
                  {displayNoMatchFoundNot && (
                    <>
                      <div className="notification is-success">
                        <p style={{ color: "white" }}>No Match Found.</p>
                      </div>
                    </>
                  )}
                  {invalidFileExtension && (
                    <div className="notification is-warning">
                      <p style={{ color: "#cc3300" }}>
                        Only Image Files Are Allowed.
                      </p>
                      <p style={{ color: "#cc3300" }}>
                        (png, jpg, jpeg and JPG)
                      </p>
                    </div>
                  )}
                  {emptySearchWarning && (
                    <div className="notification is-warning">
                      <p style={{ color: "#cc3300" }}>
                        Try Type In Something :)
                      </p>
                    </div>
                  )}
                  {displayFemaleWarning && (
                    <div className="notification is-warning">
                      <p style={{ color: "#cc3300" }}>
                        Only Men Can Be Searched.
                      </p>
                    </div>
                  )}
                  <p className="title">
                    <img src={Logo} alt="offenderbook logo" />
                  </p>

                  <form onSubmit={handleSubmit} className="form">
                    <div className="field">
                      <div className="control has-icons-right">
                        <input
                          onChange={e => setKeyword(e.target.value)}
                          className="input is-medium is-rounded"
                          type="text"
                          placeholder="i.e. Darth Vader"
                          value={keyword}
                          autoFocus="autofocus"
                        />

                        <span className="icon is-small is-right">
                          <button
                            type="submit"
                            className="button is-rounded is-outlined"
                            id="search_btn"
                          >
                            <FcSearch />
                          </button>
                        </span>
                      </div>
                    </div>
                    <br />
                      <p style={{marginLeft: '35px', color: "white", fontSize: "20px" }}>
                      <a
                        href="https://www.whydonate.eu/fundraising/offenderbook/en"
                          target="_blank" id='fundraising_link'>
                        Support
                      </a>
                      &nbsp;Stress Beacon Today !{" "}
                      <GiAerialSignal
                        style={{ height: "30px", width: "auto" }}
                      />
                    </p>
                    {sessionUsername && (
                      <button
                        type="button"
                        style={{ marginLeft: "29%" }}
                        className="button is-rounded is-outlined"
                        onClick={() => handleSecondSwitchToAddOffenderForm()}
                      >
                        <MdPersonAdd />
                        &nbsp;&nbsp;Add Offender
                      </button>
                    )}
                    <br />
                    <br />

                    <div className="file is-medium is-white is-centered">
                      {checking && (
                        <>
                          <button className="button is-loading">Loading</button>
                        </>
                      )}
                      {!checking && (
                        <>
                          <label className="file-label">
                            <input
                              className="file-input"
                              type="file"
                              name="offender_picture"
                              onChange={e => setFile(e.target.files[0])}
                            />
                            <span className="file-cta">
                              <span className="file-icon">
                                {!checking &&
                                  !invalidFileExtension &&
                                  !displayFemaleWarning && <BsUpload />}
                                {displayFemaleWarning ||
                                  (invalidFileExtension && (
                                    <BsExclamationTriangle />
                                  ))}
                              </span>
                              <span className="file-label">Upload</span>
                            </span>
                          </label>
                        </>
                      )}
                    </div>
                  </form>
                </div>
                <div className="column is-one-third"></div>
              </div>
            </>
          )}
          {displayAddOffenderForm && (
            <AddOffenderForm switchForm={setAddOffenderForm} />
          )}
        </>
      )}
      {displayMatch && !displayKeywordMatch && (
        <Profile
          offender_id={matchFound.id}
          firstName={matchFound.first_name}
          middleName={matchFound.middle_name}
          lastName={matchFound.last_name}
          comments={comments}
          onSwitchBack={handleSwitchingBack}
          firstPhoto={firstPhoto}
          secondPhoto={secondPhoto}
          thirdPhoto={thirdPhoto}
          triggerNotification={displayFoundMatchNot}
          triggerCommentsReload={displayProfileID}
        />
      )}
      {!displayProfile && displayKeywordMatch && (
        <>
          <div
            className="container"
            style={{ marginTop: "30px", width: "50%" }}
          >
            {displayNoMatchFoundNot && (
              <>
                <div className="notification is-success">
                  <p style={{ color: "white" }}>No Match Found.</p>
                </div>
              </>
            )}
            {displayFemaleWarning && (
              <>
                <div className="notification is-warning">
                  <p style={{ color: "#cc3300" }}>Only Men Can Be Searched.</p>
                </div>
              </>
            )}
            {displayHint && (
              <>
                <img src={Arrow} style={{ marginLeft: "100px" }} />
                <br />
                <br />
                <img src={Typing} style={{ marginLeft: "100px" }} />
              </>
            )}
            {keywordMatches.map((match, index) => {
              const image_file = `/offenders/fetch_one_image/${match.id}`;
              return (
                <>
                  <div
                    className="box"
                    id="pointerID"
                    key={index}
                    onClick={e => displayProfileID(match.id)}
                  >
                    <article className="media">
                      <div className="media-left">
                        <img
                          src={image_file}
                          alt="Image"
                          style={{ borderRadius: "10px" }}
                        />
                      </div>
                      <div className="media-content">
                        <div className="content">
                          <p id="name_to_check" style={{ fontSize: "20px" }}>
                            <strong>
                              {match.first_name}&nbsp;&nbsp;{match.middle_name}
                              &nbsp;&nbsp;
                              {match.last_name}
                            </strong>
                          </p>
                        </div>
                      </div>
                    </article>
                  </div>
                </>
              );
            })}
          </div>
        </>
      )}
      {displayProfile && (
        <Profile
          offender_id={selectedProfileID}
          firstName={firstName}
          middleName={middleName}
          lastName={lastName}
          comments={comments}
          onSwitchBack={handleSwitchingBack}
          firstPhoto={firstPhoto}
          secondPhoto={secondPhoto}
          thirdPhoto={thirdPhoto}
          triggerCommentsReload={displayProfileID}
        />
      )}
      <div
        id="cookie_bar"
        className="is-fixed-bottom"
        style={{ backgroundColor: "#f63c58" }}
      >
        <p>
          By using offenderbook you're agreeing with its{" "}
          <Link
            to="/terms"
            style={{ textDecoration: "underline", color: "white" }}
            alt="link to terms and conditions"
          >
            terms
          </Link>{" "}
          and{" "}
          <Link
            to="/privacy"
            style={{ textDecoration: "underline", color: "white" }}
            alt="link to privacy statement"
          >
            privacy policies
          </Link>
          . &nbsp;&nbsp;
          <button
            className="button is-small is-rounded is-outlined"
            id="cookie_btn"
            onClick={() => handleConsent()}
          >
            O&nbsp;K
          </button>
        </p>
      </div>
    </>
  );
};

export default Main;
