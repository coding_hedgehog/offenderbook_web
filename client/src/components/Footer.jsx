import React from "react";
import { Link } from "react-router-dom";
import { useGlobalContext } from "./context";

const Footer = () => {
  const { trace } = useGlobalContext();

  return (
    <footer className="footer">
      <div class="content has-text-centered">
        {trace !== "/terms" && (
          <>
            <Link
              to="/terms"
              alt="link to terms and conditions"
              style={{ color: "white" }}
            >
              Terms & Conditions
            </Link>
            &nbsp;&nbsp;&nbsp;&nbsp;
          </>
        )}
        {trace !== "/privacy" && (
          <Link
            to="/privacy"
            alt="link to privacy statement"
            style={{ color: "white" }}
          >
            Privacy Statement
          </Link>
        )}
      </div>
    </footer>
  );
};

export default Footer;
