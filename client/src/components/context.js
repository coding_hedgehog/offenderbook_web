import React, { useState, useContext } from "react";

const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  const [sessionUsername, setSessionUsername] = useState("");
  const [sessionUsernameID, setSessionUsernameID] = useState("");
  const [justLoggedOutNotif, setJustLoggedOutNotif] = useState(false);
  const [justLoggedIn, setJustLoggedIn] = useState(false);
  const [trace, setTrace] = useState("");
  const [resetSwitch, setResetSwitch] = useState(false);
  const [searchedKeyword, setSearchedKeyword] = useState("");
  //const [refreshTrigger, setRefreshTrigger] = useState(Math.random());

  return (
    <AppContext.Provider
      value={{
        sessionUsername,
        setSessionUsername,
        setSessionUsernameID,
        sessionUsernameID,
        justLoggedOutNotif,
        setJustLoggedOutNotif,
        //setRefreshTrigger,
        setSearchedKeyword,
        searchedKeyword,
        justLoggedIn,
        setJustLoggedIn,
        trace,
        setTrace,
        resetSwitch,
        setResetSwitch
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useGlobalContext = () => {
  return useContext(AppContext);
};

export { AppContext, AppProvider };
