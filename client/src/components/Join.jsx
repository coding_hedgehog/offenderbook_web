import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Redirect } from "react-router-dom";
import { FcCheckmark } from "react-icons/fc";
import { RiErrorWarningLine } from "react-icons/ri";
import { HiArrowNarrowRight } from "react-icons/hi";
import {
  BsExclamationTriangle,
  BsUpload,
  BsFillLockFill
} from "react-icons/bs";
import { CgAsterisk } from "react-icons/cg";
import { MdEmail } from "react-icons/md";
import { FaUserAlt } from "react-icons/fa";
import { GiSandsOfTime } from "react-icons/gi";
import { useGlobalContext } from "./context";
import { Link } from "react-router-dom";
import Navbar from "./Navbar";
import axios from "axios";

const Register = () => {
  const {
    trace,
    setTrace,
    setResetSwitch,
    resetSwitch,
    sessionUsername
  } = useGlobalContext();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [displayMaleWarning, setDisplayMaleWarning] = useState(false);
  const [displayUserExistsWarning, setDisplayUserExistsWarning] = useState(
    false
  );
  const [successNotification, setSuccessNotification] = useState(false);
  const [displayWarningPhotosDontMatch, setWarningPhotosDontMatch] = useState(
    false
  );
  const [displayPasswordLengthWarning, setPasswordLengthWarning] = useState(
    false
  );
  const [passportCheckFailed, setPassportCheckFailed] = useState(false);
  const [isLikelyFakePassport, setLikelyFakePassportWarning] = useState(false);
  const [isPassportValidated, setPassportValidated] = useState(false);
  const [isPassportSelected, setPassportSelected] = useState(false);
  const [invalidFileExtension, setInvalidFileExtension] = useState(false);
  const [selfieDidNotPass, setSelfieDidNotPass] = useState(false);
  const [isCheckingPassport, setIsCheckingPassport] = useState(false);
  const [joinBtnIsDisabled, setJoinBtnDisabled] = useState(true);
  const [isWaiting, setWaiting] = useState(false);
  const [isEmailInvalid, setEmailInvalid] = useState(false);
  const [passportFile, setPassportFile] = useState();
  const [firstSelfieFile, setFirstSelfieFile] = useState();
  const [secondSelfieFile, setSecondSelfieFile] = useState();
  const [thirdSelfieFile, setThirdSelfieFile] = useState();

  const passwordStylingOn = { fontFamily: "Orbitron" };
  const passwordStylingOff = { fontFamily: "Verdana" };

  const handleSelfieUpload = e => {
    if (e.target.name === "first_selfie") {
      setFirstSelfieFile(e.target.files[0]);
    } else if (e.target.name === "second_selfie") {
      setSecondSelfieFile(e.target.files[0]);
    } else if (e.target.name === "third_selfie") {
      setThirdSelfieFile(e.target.files[0]);
    }
  };

  const handleSubmit = async e => {
    e.preventDefault();

    if (password.length < 8) {
      setPasswordLengthWarning(true);
    } else {
      if (
        !email.includes("@icloud") &&
        !email.includes("@yahoo") &&
        !email.includes("@gmail") &&
        !email.includes("@protonmail") &&
        !email.includes("@outlook") &&
        !email.includes("@live") &&
        !email.includes("@msn") &&
        !email.includes("@mail") &&
        !email.includes("@pm.me") &&
        !email.includes("@virginmedia") &&
        !email.includes("@startmail")
      ) {
        setEmailInvalid(true);
        document.getElementsByTagName("body")[0].style = "height: auto;";
      } else {
        setWaiting(true);
        const config = {
          headers: {
            "Content-Type": "multipart/form-data"
          }
        };
        const fd = new FormData();
        const url = "/auth/register";
        fd.append("selfie1", firstSelfieFile, firstSelfieFile.name);
        fd.append("selfie2", secondSelfieFile, secondSelfieFile.name);
        fd.append("selfie3", thirdSelfieFile, thirdSelfieFile.name);
        fd.append("passport_photo", passportFile, passportFile.name);
        fd.append("username", username);
        fd.append("email", email);
        fd.append("password", password);
        try {
          const resp = await axios.post(url, fd, config);

          if (resp.data.message.includes("successfully registered")) {
            setSuccessNotification(true);
            //console.log(resp.data.message);
          } else if (
            resp.data.message ===
            "Sorry. Your photo don't match the photo in the passport."
          ) {
            setPassportCheckFailed(true);
          } else if (resp.data.message === "this user exists.") {
            setDisplayUserExistsWarning(true);
          } else {
            setSelfieDidNotPass(true);
          }
        } catch (err) {
          setWaiting(false);
          setWarningPhotosDontMatch(true);
        }

        setWaiting(false);
        setUsername("");
        setEmail("");
        setPassword("");
        setPassportFile("");
        setFirstSelfieFile("");
        setSecondSelfieFile("");
        setThirdSelfieFile("");
        setPassportValidated(false);
      }
    }
  };

  const checkPassport = () => {
    if (passportFile) {
      setIsCheckingPassport(true);
      const fd = new FormData();
      fd.append("image", passportFile, passportFile.name);
      const url = "/auth/check_passport";
      const headers = {
        "Content-Type": "multipart/form-data"
      };

      axios
        .post(url, fd, headers)
        .then(resp => {
          setIsCheckingPassport(false);
          if (resp.data.message === "Unsupported filetype") {
            setInvalidFileExtension(true);
            setPassportFile(null);
          } else if (resp.data.message === "passed") {
            setPassportValidated(true);
          } else if (
            resp.data.message === "did not pass" ||
            resp.data.message === "not a passport"
          ) {
            setPassportCheckFailed(true);
            console.log("passport failed");
            setPassportFile(null);
          } else if (resp.data.message === "male") {
            setDisplayMaleWarning(true);
            setPassportFile(null);
          } else if (resp.data.message === "likely fake") {
            setLikelyFakePassportWarning(true);
            setPassportFile(null);
          } else if (resp.data.message === "this user exists.") {
            setDisplayUserExistsWarning(true);
            setPassportFile(null);
          } else {
            console.log(resp.data.message);
          }
        })
        .catch(e => {
          setPassportCheckFailed(true);
          console.log(e);
        });
    }
  };

  useEffect(() => {
    checkPassport();
  }, [passportFile]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDisplayUserExistsWarning(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [displayUserExistsWarning]);

  useEffect(() => {
    if (selfieDidNotPass) {
      const timeout = setTimeout(() => {
        setSelfieDidNotPass(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [selfieDidNotPass]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setInvalidFileExtension(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [invalidFileExtension]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setPasswordLengthWarning(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [displayPasswordLengthWarning]);

  useEffect(() => {
    if (
      username &&
      email &&
      password &&
      firstSelfieFile &&
      secondSelfieFile &&
      thirdSelfieFile &&
      isPassportValidated
    ) {
      setJoinBtnDisabled(false);
    } else {
      setJoinBtnDisabled(true);
    }
  });

  useEffect(() => {
    const timeout = setTimeout(() => {
      setSuccessNotification(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [successNotification]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setWarningPhotosDontMatch(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [displayWarningPhotosDontMatch]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setPassportCheckFailed(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [passportCheckFailed]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDisplayMaleWarning(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [displayMaleWarning]);

  useEffect(() => {
    if (isLikelyFakePassport) {
      const timeout = setTimeout(() => {
        setLikelyFakePassportWarning(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [isLikelyFakePassport]);

  useEffect(() => {
    if (isEmailInvalid) {
      const timeout = setTimeout(() => {
        setEmailInvalid(false);
        document.getElementsByTagName("body")[0].style = "height: auto";
      }, 10000);
      return () => clearTimeout(timeout);
    }
  }, [isEmailInvalid]);

  useEffect(() => {
    setUsername("");
    setEmail("");
    setPassword("");
    setPassportFile("");
    setFirstSelfieFile("");
    setSecondSelfieFile("");
    setThirdSelfieFile("");
    setTrace("/join");
    setResetSwitch(true);
  }, [!resetSwitch]);

  useEffect(() => {
    document.getElementsByTagName(
      "body"
    )[0].style = `height: ${window.screen.height}px`;
    document.getElementsByTagName("title")[0].innerHTML = "Join Offenderbook";
    document
      .querySelector('meta[name="description"]')
      .setAttribute("content", "Join Offenderbook");
  }, []);

  return (
    <>
      {sessionUsername && <Redirect to="/" />}
      <Navbar />
      <div className="columns">
        <div className="column is-one-third">
          <div
            className="notification is-info"
            style={{ marginLeft: "20px", marginTop: "300px", width: "80%" }}
          >
            <strong>
              <RiErrorWarningLine size={30} />
              <br />
              Please use your phone camera to take selfies. Images downloaded
              from the Internet won't be
              accepted.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <HiArrowNarrowRight size={20} />
            </strong>
          </div>
        </div>
        <div className="column is-one-third">
          <form onSubmit={handleSubmit} id="register_form">
            {displayMaleWarning && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  Sorry, only women can create an account.
                </p>
              </div>
            )}
            {displayUserExistsWarning && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  You seem to have an account already. Sign in instead.
                </p>
              </div>
            )}
            {selfieDidNotPass && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  Sorry, your selfie(s) didn't pass our check. Try take another
                  one.
                </p>
              </div>
            )}
            {isLikelyFakePassport && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  Sorry, selected passport doesn't appear to be valid.
                </p>
              </div>
            )}
            {invalidFileExtension && (
              <div className="notification is-danger">
                <p style={{ color: "#cc3300" }}>
                  Only image types .png, .jpeg, .jpg and .JPG are allowed.
                </p>
              </div>
            )}
            {successNotification && (
              <div className="notification is-success">
                <p style={{ color: "white" }}>
                  Almost done ! Now check your inbox and confirm email we've
                  sent you.
                </p>
              </div>
            )}
            {displayWarningPhotosDontMatch && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  Sorry, your photo doesn't seem to match photo in the passport.
                </p>
              </div>
            )}
            {isEmailInvalid && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  Sorry, only the below email domains are allowed:
                </p>
                <ul style={{ marginTop: "10px" }}>
                  <li>@icloud</li>
                  <li>@yahoo</li>
                  <li>@protonmail (also pm.me)</li>
                  <li>@btinternet</li>
                  <li>@gmail</li>
                  <li>@outlook</li>
                  <li>@live</li>
                  <li>@mail</li>
                  <li>@msn</li>
                  <li>@virginmedia</li>
                  <li>@startmail</li>
                </ul>
              </div>
            )}

            {passportCheckFailed && (
              <div className="notification is-warning">
                <p>
                  Sorry, passport check failed. If the picture of passport or
                  selfie is blurry try take another one.
                </p>
              </div>
            )}
            <h5 className="title is-5" style={{ color: "white" }}>
              Join
            </h5>
            <div className="field">
              <div className="control has-icons-left has-icons-right">
                <input
                  onChange={e => setUsername(e.target.value)}
                  className="input is-rounded is-focused"
                  type="text"
                  placeholder="Username"
                  value={username}
                  id="required"
                  autoFocus="autofocus"
                />
                <span className="icon is-small is-left">
                  <FaUserAlt />
                </span>
                <span className="icon is-small is-right">
                  <CgAsterisk />
                </span>
              </div>
            </div>
            <div className="field">
              <div className="control has-icons-left has-icons-right">
                <input
                  onChange={e => setEmail(e.target.value)}
                  className="input is-rounded"
                  type="email"
                  placeholder="@"
                  value={email}
                  id="required"
                />
                <span className="icon is-small is-left">
                  <MdEmail />
                </span>
                <span className="icon is-small is-right">
                  <CgAsterisk />
                </span>

                {isEmailInvalid && (
                  <p
                    className="help is-danger"
                    style={{ color: "white", fontSize: "17px" }}
                  >
                    This email is invalid
                  </p>
                )}
              </div>
            </div>
            <div className="field">
              <div className="control has-icons-left has-icons-right">
                <input
                  onChange={e => setPassword(e.target.value)}
                  style={
                    password.length === 0
                      ? passwordStylingOn
                      : passwordStylingOff
                  }
                  className="input is-rounded"
                  type="password"
                  placeholder="Password"
                  value={password}
                  id="required"
                />
                <span className="icon is-small is-left">
                  <BsFillLockFill />
                </span>
                <span className="icon is-small is-right">
                  <CgAsterisk />
                </span>
                {displayPasswordLengthWarning && (
                  <p
                    className="help is-warning"
                    style={{ color: "white", fontSize: "17px" }}
                  >
                    Your Password Needs To Be At Least 8 Characters Long
                  </p>
                )}
              </div>
            </div>
            <div className="file">
              <label className="file-label">
                <input
                  onChange={e => setPassportFile(e.target.files[0])}
                  className="file-input"
                  type="file"
                  name="passport_photo"
                  id="required"
                />
                <span className="file-cta">
                  <span className="file-icon">
                    {isCheckingPassport && <GiSandsOfTime />}
                    {!passportFile && !isPassportValidated && <BsUpload />}
                    {isPassportValidated && <FcCheckmark />}
                    {!isCheckingPassport &&
                      !isPassportValidated &&
                      passportFile && <BsExclamationTriangle />}
                  </span>
                  <span className="file-label">Upload Passport Photo</span>
                </span>
              </label>
            </div>

            <small style={{ color: "white" }}>
              We don't keep picture of your passport. Its only for validation
              purposes.
            </small>
            <br />
            <br />
            <div className="file">
              <label className="file-label">
                <input
                  onChange={e => handleSelfieUpload(e)}
                  className="file-input"
                  type="file"
                  name="first_selfie"
                  id="required"
                />
                <span className="file-cta">
                  <span className="file-icon">
                    {!firstSelfieFile && <BsUpload />}
                    {firstSelfieFile && <FcCheckmark />}
                  </span>
                  <span className="file-label">Upload First Selfie</span>
                </span>
              </label>
            </div>
            <div className="file" style={{ marginTop: "5px" }}>
              <label className="file-label">
                <input
                  onChange={e => handleSelfieUpload(e)}
                  className="file-input"
                  type="file"
                  name="second_selfie"
                  id="required"
                />
                <span className="file-cta">
                  <span className="file-icon">
                    {!secondSelfieFile && <BsUpload />}
                    {secondSelfieFile && <FcCheckmark />}
                  </span>
                  <span className="file-label">Upload Second Selfie</span>
                </span>
              </label>
            </div>
            <div className="file" style={{ marginTop: "5px" }}>
              <label className="file-label">
                <input
                  onChange={e => handleSelfieUpload(e)}
                  className="file-input"
                  type="file"
                  name="third_selfie"
                  id="required"
                />
                <span className="file-cta">
                  <span className="file-icon">
                    {!thirdSelfieFile && <BsUpload />}
                    {thirdSelfieFile && <FcCheckmark />}
                  </span>
                  <span className="file-label">Upload Third Selfie</span>
                </span>
              </label>
            </div>
            <br />
            <div className="field is-grouped">
              <div className="control">
                {isWaiting && (
                  <button className="button is-danger is-loading">
                    Loading
                  </button>
                )}
                {!isWaiting && !joinBtnIsDisabled && (
                  <button className="button is-rounded is-outline">Join</button>
                )}
                {joinBtnIsDisabled && (
                  <button className="button is-rounded is-outline" disabled>
                    Join
                  </button>
                )}
              </div>
              <Link
                to="/"
                id="cancel_link"
                className="button is-rounded is-outline"
              >
                Cancel
              </Link>
            </div>
          </form>
        </div>
        <div className="column is-one-third"></div>
      </div>
    </>
  );
};

export default Register;
