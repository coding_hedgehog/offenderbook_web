import React from "react";
import { shallow } from "enzyme";
import Main from "../Main";
import { useGlobalContext } from "../context";

test("Landing page renders without crashing", () => {
  const wrapper = shallow(<Main />);
  const nav_element = wrapper.find("nav");
  const section_element = wrapper.find("section");
  const p_elements = wrapper.find("p");
  const form_element = wrapper.find("form");
  const btn_element = wrapper.find("button");
  expect(nav_element.length).toBe(1);
  expect(section_element.length).toBe(1);
  expect(p_elements.length).toBe(2);
  expect(form_element.length).toBe(1);
  expect(btn_element.length).toBe(1);
});
