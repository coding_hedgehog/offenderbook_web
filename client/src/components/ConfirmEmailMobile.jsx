import React, { useState, useEffect } from "react";
import { useGlobalContext } from "./context";
import axios from "axios";

const ConfirmEmailMobile = ({ match }) => {
  const { setTrace } = useGlobalContext();
  const { auth_string } = match.params;
  const [successResponse, setSuccessResponse] = useState(false);
  const [failureResponse, setFailureResponse] = useState(false);
  const [alreadyConfirmedNotif, setAlreadyConfirmedNotif] = useState(false);

  useEffect(() => {
    const url = `/auth/confirm_email/${auth_string}`;
    axios
      .get(url)
      .then(resp => {
        if (resp.data.message === "email confirmed.") {
          setSuccessResponse(true);
          console.log('CONFIRMED');
        } else if (
          resp.data.message === "this email has been already confirmed."
        ) {
          setAlreadyConfirmedNotif(true);
        } else {
          console.log(resp.data.message);
          }
      })
      .catch(err => {
        setFailureResponse(true);
        console.log(err);
      });
  }, [auth_string]);

  return (
    <>
      <div className="container" style={{ marginTop: "40px" }}>
        {successResponse && (
          <div className="notification is-success">
            <p>
              Email Confirmed ! Sweet ! You can now close the browser and log
              into the app.
            </p>
          </div>
        )}
        {failureResponse && (
          <div className="notification is-warning">
            <p>Sorry, something went wrong ...</p>
          </div>
        )}
        {alreadyConfirmedNotif && (
          <div className="notification is-info">
            <p>
              This email has been already confirmed. No need to do it again.
            </p>
          </div>
        )}
      </div>
    </>
  );
};

export default ConfirmEmailMobile;
