import React, { useState, useEffect } from "react";
import { useGlobalContext } from "./context";
import { FcCheckmark } from "react-icons/fc";
import { BsExclamationTriangle, BsUpload } from "react-icons/bs";
import Hint1 from "../assets/hint1.png";
import Hint2 from "../assets/hint2.png";
import Hint3 from "../assets/hint3.png";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";

const AddOffenderForm = props => {
  const { sessionUsernameID, sessionUsername } = useGlobalContext();
  const [firstName, setFirstName] = useState("");
  const [middleName, setMiddleName] = useState("");
  const [lastName, setLastName] = useState("");
  const [comment, setComment] = useState("");
  const [firstPhoto, setFirstPhoto] = useState();
  const [secondPhoto, setSecondPhoto] = useState();
  const [thirdPhoto, setThirdPhoto] = useState();
  const [isPhotoFileInvalid, setPhotoFileInvalid] = useState(false);
  const [displayInvalidFileWarning, setInvalidFileExtension] = useState(false);
  const [addBtnIsDisabled, setAddBtnDisabled] = useState(true);
  const [displayGenderWarning, setDisplayGenderWarning] = useState(false);
  const [displaySpacesWarning, setDisplaySpacesWarning] = useState(false);
  const [displayOffenderExists, setOffenderExistsWarning] = useState(false);
  const [displayImageError, setDisplayImageError] = useState(false);
  const [displaySuccessNot, setSuccessNotification] = useState(false);
  const [profanityTrigger, setProfanityTrigger] = useState(false);
  const [
    displayCharacterLengthWarning,
    setDisplayCharacterLengthWarning
  ] = useState(false);
  const [isWaiting, setWaiting] = useState(false);
  const { switchForm } = props;

  const handleSubmit = async e => {
    e.preventDefault();
    if (comment.length < 25) {
      setDisplayCharacterLengthWarning(true);
    } else if (
      comment.includes("fuck") ||
      comment.includes("suck") ||
      comment.includes("f**k") ||
      comment.includes("s**k") ||
      comment.includes("shit") ||
      comment.includes("s**t") ||
      comment.includes("sh*t") ||
      comment.includes("fu*k") ||
      comment.includes("ass") ||
      comment.includes("asshole") ||
      comment.includes("a**hole") ||
      comment.includes("jerk")
    ) {
      setProfanityTrigger(true);
    } else if (
      firstName.includes(" ") ||
      middleName.includes(" ") ||
      lastName.includes(" ")
    ) {
      setDisplaySpacesWarning(true);
    } else {
      setWaiting(true);
      const headers = {
        "Content-Type": "multipart/form-data"
      };

      const fd = new FormData();
      const url = "/offenders/add";
      fd.append("first_name", firstName);
      fd.append("middle_name", middleName);
      fd.append("last_name", lastName);
      fd.append("image_file1", firstPhoto, firstPhoto.name);
      fd.append("image_file2", secondPhoto, secondPhoto.name);
      fd.append("image_file3", thirdPhoto, thirdPhoto.name);
      fd.append("new_comment", comment);
      fd.append("userID", sessionUsernameID);
      try {
        const resp = await axios.post(url, fd, headers);

        if (resp.data.message === "offender added") {
          setSuccessNotification(true);
        } else if (resp.data.message === "This man is already registered.") {
          setOffenderExistsWarning(true);
        } else if (resp.data.message === "Only image files are allowed") {
          setInvalidFileExtension(true);
        } else if (resp.data.message === "Only men can be added.") {
          setDisplayGenderWarning(true);
        } else if (resp.data.message === "not a face") {
          setDisplayImageError(true);
        } else {
          console.log(resp.data.message);
        }
      } catch (err) {
        console.log(err);
      }
      setFirstName("");
      setMiddleName("");
      setLastName("");
      setComment("");
      setFirstPhoto("");
      setSecondPhoto("");
      setThirdPhoto("");
    }

    setWaiting(false);
  };

  const handlePhotoUpload = e => {
    if (e.target.name === "offender_photo1") {
      setFirstPhoto(e.target.files[0]);
    } else if (e.target.name === "offender_photo2") {
      setSecondPhoto(e.target.files[0]);
    } else if (e.target.name === "offender_photo3") {
      setThirdPhoto(e.target.files[0]);
    }
  };

  useEffect(() => {
    if (profanityTrigger) {
      const timeout = setTimeout(() => {
        setProfanityTrigger(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [profanityTrigger]);

  useEffect(() => {
    if (displaySpacesWarning) {
      const timeout = setTimeout(() => {
        setDisplaySpacesWarning(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [displaySpacesWarning]);

  useEffect(() => {
    if (displayCharacterLengthWarning) {
      const timeout = setTimeout(() => {
        setDisplayCharacterLengthWarning(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [displayCharacterLengthWarning]);

  useEffect(() => {
    console.log(sessionUsernameID);
    // console.log(sessionUsername);
    const timeout = setTimeout(() => {
      setInvalidFileExtension(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [displayInvalidFileWarning]);

  useEffect(() => {
    if (firstPhoto && secondPhoto && thirdPhoto && comment) {
      setAddBtnDisabled(false);
    } else {
      setAddBtnDisabled(true);
    }
  });

  useEffect(() => {
    const timeout = setTimeout(() => {
      setOffenderExistsWarning(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [displayOffenderExists]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDisplayGenderWarning(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [displayGenderWarning]);

  useEffect(() => {
    if (displaySuccessNot) {
      document.getElementsByTagName("body")[0].style = `height: ${window.screen
        .height + 30}px`;
      const timeout = setTimeout(() => {
        setSuccessNotification(false);
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [displaySuccessNot]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDisplayImageError(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [displayImageError]);

  return (
    <>
      <div className="columns">
        <div className="column is-one-third" style={{ textAlign: "center" }}>
          <img
            style={{ marginRight: "5px", marginTop: "150px" }}
            src={Hint1}
            alt="first hint for correct picture"
          />
          <img
            style={{ marginRight: "5px" }}
            src={Hint2}
            alt="second hint for correct picture"
          />
          <img src={Hint3} alt="hint for incorrect picture" />
        </div>
        <div className="column is-one-third">
          <form onSubmit={handleSubmit} id="add_form">
            {displayInvalidFileWarning && (
              <div className="notification is-warning">
                <p style={{ color: "#f63c58" }}>
                  Only Image Files Are Allowed.
                </p>
              </div>
            )}
            {displayOffenderExists && (
              <div className="notification is-warning">
                <p style={{ color: "#f63c58" }}>
                  This Man Is Already Registered.
                </p>
              </div>
            )}
            {displayGenderWarning && (
              <div className="notification is-warning">
                <p style={{ color: "#f63c58" }}>Only Men Can Be Added.</p>
              </div>
            )}
            {displayImageError && (
              <div className="notification is-warning">
                <p style={{ color: "#f63c58" }}>
                  Image Error. If The Image Is Blurry Try Upload Another One.
                </p>
              </div>
            )}
            {displayCharacterLengthWarning && (
              <div
                className="notification is-warning"
                style={{
                  width: "90%",
                  marginTop: "20px"
                }}
              >
                <p style={{ color: "#cc3300", fontWeight: "bold" }}>
                  New Warning Needs To Be At Least 25 Characters Long.
                </p>
              </div>
            )}
            {profanityTrigger && (
              <div
                className="notification is-warning"
                style={{
                  width: "90%",
                  marginTop: "20px"
                }}
              >
                <p style={{ color: "#cc3300", fontWeight: "bold" }}>
                  No Swear Words Are Allowed. Let's Keep Things Decent :)
                </p>
              </div>
            )}
            {displaySuccessNot && (
              <div className="notification is-success">
                <p style={{ color: "white" }}>Offender Added.</p>
              </div>
            )}
            {displaySpacesWarning && (
              <div className="notification is-warning">
                <p style={{ color: "#cc3300" }}>
                  No Spaces In Name Fields Are Allowed
                </p>
              </div>
            )}
            <h5 className="title is-5" style={{ color: "white" }}>
              Add Offender
            </h5>
            <div className="field">
              <div className="control">
                <input
                  onChange={e => setFirstName(e.target.value)}
                  className="input is-rounded is-focused"
                  name="first_name"
                  type="text"
                  placeholder="First Name (optional)"
                  value={firstName}
                  autoFocus="autofocus"
                />
                <small style={{ color: "white", marginLeft: "10px" }}>
                  Real name only. No nicknames.
                </small>
              </div>
            </div>
            <div className="field">
              <div className="control">
                <input
                  onChange={e => setMiddleName(e.target.value)}
                  className="input is-rounded"
                  name="middle_name"
                  type="text"
                  placeholder="Middle Name (optional)"
                  value={middleName}
                />
              </div>
            </div>
            <div className="field">
              <div className="control">
                <input
                  onChange={e => setLastName(e.target.value)}
                  className="input is-rounded"
                  name="last_name"
                  type="text"
                  placeholder="Last Name (optional)"
                  value={lastName}
                />
              </div>
            </div>
            <br />
            <div className="file">
              <label className="file-label">
                <input
                  onChange={e => handlePhotoUpload(e)}
                  className="file-input"
                  type="file"
                  name="offender_photo1"
                  id="required"
                />
                <span className="file-cta">
                  <span className="file-icon">
                    {!firstPhoto && <BsUpload />}
                    {firstPhoto && <FcCheckmark />}
                    {isPhotoFileInvalid && <BsExclamationTriangle />}
                  </span>
                  <span className="file-label">Upload 1st Photo</span>
                </span>
              </label>
            </div>
            <br />
            <div className="file">
              <label className="file-label">
                <input
                  onChange={e => handlePhotoUpload(e)}
                  className="file-input"
                  type="file"
                  name="offender_photo2"
                  id="required"
                />
                <span className="file-cta">
                  <span className="file-icon">
                    {!secondPhoto && <BsUpload />}
                    {secondPhoto && <FcCheckmark />}
                    {isPhotoFileInvalid && <BsExclamationTriangle />}
                  </span>
                  <span className="file-label">Upload 2nd Photo</span>
                </span>
              </label>
            </div>
            <br />
            <div className="file">
              <label className="file-label">
                <input
                  onChange={e => handlePhotoUpload(e)}
                  className="file-input"
                  type="file"
                  name="offender_photo3"
                  id="required"
                />
                <span className="file-cta">
                  <span className="file-icon">
                    {!thirdPhoto && <BsUpload />}
                    {thirdPhoto && <FcCheckmark />}
                    {isPhotoFileInvalid && <BsExclamationTriangle />}
                  </span>
                  <span className="file-label">Upload 3rd Photo</span>
                </span>
              </label>
            </div>
            <br />
            <div className="field">
              <div className="control">
                <label style={{ color: "white" }}>Warning:</label>&nbsp;&nbsp;
                <span style={{ color: "white" }}>*</span>
                <textarea
                  onChange={e => setComment(e.target.value)}
                  className="input"
                  name="comment"
                  rows="4"
                  style={{ height: "100px" }}
                  placeholder="Gets very aggressive after having had 3 beers, relationship with him was emotionally abusive, stalkerish ..."
                  value={comment}
                />
              </div>
            </div>

            <div className="field is-grouped">
              {isWaiting && (
                <button className="button is-loading">Loading</button>
              )}
              {!isWaiting && !addBtnIsDisabled && (
                <div className="control">
                  <button className="button is-rounded is-outline">Add</button>
                </div>
              )}
              {!isWaiting && addBtnIsDisabled && (
                <div className="control">
                  <button className="button is-rounded is-outline" disabled>
                    Add
                  </button>
                </div>
              )}

              <div className="control">
                <Link
                  to="#"
                  id="cancel_link"
                  className="button is-link is-rounded"
                  onClick={() => switchForm(false)}
                >
                  Cancel
                </Link>
              </div>
            </div>
          </form>
        </div>
        <div className="column is-one-third"></div>
      </div>
    </>
  );
};

export default AddOffenderForm;
