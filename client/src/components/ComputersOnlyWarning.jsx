import React, { useEffect } from "react";
import GooglePlayLogo from "../assets/google_play.png";

const ComputersOnly = () => {
  useEffect(() => {
    let width = window.screen.width;
    let margin = `${width / 2 - 40}px`;
    document.getElementById(
      "google_logo"
    ).style = `width: 20%;height: auto; margin-left: ${margin}`;
  }, []);

  return (
    <>
      <h6
        className="title is-6"
        style={{ color: "white", marginLeft: "10px", paddingTop: "10px" }}
      >
        Hi, Android App is now in the stage of testing and we're looking for
        female testers. If you would like to participate in testing fill out{" "}
        <a
          style={{ color: "white", textDecoration: "underline" }}
          href="https://forms.gle/hk297b7ZbZfKyjXf8"
          target="_blank"
          alt="link to sign up form"
        >
          this
        </a>
        &nbsp;form.
      </h6>
      <br />
      <br />
      <img src={GooglePlayLogo} id="google_logo" />
    </>
  );
};

export default ComputersOnly;
