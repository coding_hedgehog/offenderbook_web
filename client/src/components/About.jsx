import React, { useState, useEffect } from "react";
import { useGlobalContext } from "./context";
import { Link, Redirect } from "react-router-dom";
import Navbar from "./Navbar";

const About = () => {
  const { setTrace } = useGlobalContext();
  const [backToHome, setBack] = useState(false);

  useEffect(() => {
    setTrace("/about");
    document.getElementsByTagName("body")[0].style = "height: auto";
    document.getElementsByTagName('title')[0].innerHTML = 'About Offenderbook';
    document
      .querySelector('meta[name="description"]')
      .setAttribute("content", "What We Are Up To");
  }, []);

  return (
    <>
      {backToHome && <Redirect to="/" />}
      <Navbar displayLogo={"true"} />
      <div className="hero">
        <div className="hero-body">
          <h1 className="title is-1" style={{ color: "white" }}>
            What We're Up To
            <button
              style={{ marginLeft: "80px" }}
              className="delete is-large"
              onClick={() => setBack(true)}
            ></button>
          </h1>

          <div
            className="container"
            style={{ textAlign: "left", marginTop: "100px" }}
          >
            <p className="about_p">
              Offenderbook has been created to address the problem of increasing
              numbers of abusive relationships. Only after reading first few
              bullet points of{" "}
              <a
                href="http://localhost:5000/offenders/download_report"
                alt="download domestic violence report"
                style={{
                  color: "white",
                  textDecoration: "underline"
                }}
              >
                this
              </a>{" "}
              report which is dated between 2016 - 2018 (we have no reason to
              think it got any better ever since) you will understand what
              Offenderbook is trying to accomplish. We at Offenderbook aren't
              trying to completely eliminate domestic violence, but with your
              help we strongly believe numbers of abusive relationships can
              drastically drop.
            </p>
            <br />
            <br />

            <p className="about_p">
              We consider ourselves to be a complementary service to online
              dating apps. We're merely trying to help to do their job better by
              informing you if your date is safe to go out with.
            </p>
            <br />
            <br />
            <p className="about_p">
              How would you feel knowing that your teenage daughter isn't going
              out with a man who's got history of not treating women right ? Not
              all men who treat women badly end up in prison.
            </p>

            <p className="about_p">
              Here comes a big disclaimer. This is Offenderbook, not Shamebook.
              Women are highly encouraged to be{" "}
              <span style={{ textDecoration: "underline", fontWeight: "bold" }}>
                genuine
              </span>{" "}
              and{" "}
              <span style={{ textDecoration: "underline", fontWeight: "bold" }}>
                truthful
              </span>
              . That's why we designed Offenderbook in such a way that it takes
              2 women (ideally related or close friends) to add a man to
              Offenderbook. One woman who adds a warning about the man who is
              abusive and another woman who testifies that the warning is true.
              If the warning won't be confirmed by another woman within 24 hours
              it will be automatically deleted and won't get added.
            </p>
            <br />
            <br />

            <p className="about_p">
              Offenderbook hasn't been created to condemn men who treat women
              badly. We believe abusive men can change. Get counselled, get help
              from a professional. That's why men who are added to Offenderbook
              will be automatically deleted from it if no woman have reported
              any new warnings about them in the last 2 years.
            </p>
            <br />
            <br />

            <p className="about_p">
              Offenderbook hasn't been created to shame men but to prevent
              potentially abusive relationships to begin.
            </p>
            <br />
            <br />
            <p className="about_p">
              Everyone can search through profiles, but for obvious reasons only
              women can create an account and add warning about a man. Passport
              and selfie check is in place.
            </p>
            <br />
            <br />
            <p className="about_p">
              Offenderbook is currently available on larger screens only
              (computers). Mobile version is on its way. :)
            </p>
            <br />
            <br />
            <h4
              className="title is-4"
              style={{ color: "white", textDecoration: "underline" }}
            >
              Values:
            </h4>
            <ul
              style={{
                color: "white",
                listStyle: "square",
                marginLeft: "30px"
              }}
            >
              <li>
                <h6 className="title is-6" style={{ color: "white" }}>
                  Woman's Safety Comes First
                </h6>
              </li>
              <br />
              <div className="content" style={{ marginLeft: "10px" }}>
                When making design decisions, question that is always asked "How
                this will increase woman's safety on dates ?"
              </div>
              <li>
                <h6 className="title is-6" style={{ color: "white" }}>
                  Never-Ending Improvement
                </h6>
              </li>
              <br />
              <div className="content" style={{ marginLeft: "10px" }}>
                Offenderbook is always open to{" "}
                <Link
                  to="/feedback"
                  alt="link to feedback form"
                  style={{ color: "white", textDecoration: "underline" }}
                >
                  feedback
                </Link>
                . Only with honest feedback offenderbook can become what is
                intented to become and improve. We don't believe we'll ever say
                "Offenderbook is now finished. Job done.".
              </div>
              <li>
                <h6 className="title is-6" style={{ color: "white" }}>
                  Godly Leadership
                </h6>
              </li>
              <br />
              <div className="content" style={{ marginLeft: "10px" }}>
                Little disclaimer here. Offenderbook isn't led by "religious"
                people. But it has been decided that decision makers are people
                who follow biblical principles.
              </div>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default About;
