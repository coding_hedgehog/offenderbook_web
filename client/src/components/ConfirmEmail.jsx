import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import Navbar from "./Navbar";
import axios from "axios";

const ConfirmEmail = ({ match }) => {
  const { auth_string } = match.params;
  const [successResponse, setSuccessResponse] = useState(false);
  const [failureResponse, setFailureResponse] = useState(false);
  const [alreadyConfirmedNotif, setAlreadyConfirmedNotif] = useState(false);
  const [triggerTimoutForRedirect, setTriggerRedirect] = useState(false);
  const [triggerRedirect, setRedirect] = useState(false);

  useEffect(() => {
    const url = `/auth/confirm_email/${auth_string}`;
    axios
      .get(url)
      .then(resp => {
        if (resp.data.message === "email confirmed.") {
          setSuccessResponse(true);
        } else if (
          resp.data.message === "this email has been already confirmed."
        ) {
          setAlreadyConfirmedNotif(true);
        }
        setTriggerRedirect(true);
      })
      .catch(err => {
        setFailureResponse(true);
        console.log(err);
      });
  }, [auth_string]);

  useEffect(() => {
    document.getElementsByTagName(
      "body"
    )[0].style = `height: ${window.screen.height}px`;
  });

  useEffect(() => {
    const timeout = setTimeout(() => {
      setRedirect(true);
    }, 5000);
    return () => clearTimeout(timeout);
  }, [triggerTimoutForRedirect]);

  return (
    <>
      {triggerRedirect && <Redirect to="/login" />}
      {!auth_string && <Redirect to="/" />}

      <Navbar displayLogo={"true"} />
      <div className="container" style={{ marginTop: "40px" }}>
        {successResponse && (
          <div className="notification is-success">
            <p>Email Confirmed ! Sweet ! We'll Now Redirect You To Login ...</p>
          </div>
        )}
        {failureResponse && (
          <div className="notification is-warning">
            <p>Sorry, something went wrong ...</p>
          </div>
        )}
        {alreadyConfirmedNotif && (
          <div className="notification is-info">
            <p>
              This email has been already confirmed. No need to do it again.
              Taking you to login ...
            </p>
          </div>
        )}
      </div>
    </>
  );
};

export default ConfirmEmail;
