import React, { useState, useEffect } from "react";
import { useGlobalContext } from "./context";
import axios from "axios";

const Profile = props => {
  const {
    offender_id,
    firstName,
    middleName,
    lastName,
    comments,
    onSwitchBack,
    firstPhoto,
    secondPhoto,
    thirdPhoto,
    triggerNotification,
    triggerCommentsReload
  } = props;
  const { sessionUsernameID, sessionUsername } = useGlobalContext();
  const [confirmedComments, setConfirmedComments] = useState(
    comments.filter(comment => comment.confirmed === true).reverse()
  );
  const [awaitingConfirmation, setAwaitingConfirmation] = useState(
    comments.filter(comment => comment.confirmed === false).reverse()
  );
  const [displayPrompt, setDisplayPrompt] = useState(false);
  const [
    displayCharacterLengthWarning,
    setDisplayCharacterLengthWarning
  ] = useState(false);
  const [newWarningNotification, setNewWarningNotification] = useState(false);
  const [profanityTrigger, setProfanityTrigger] = useState(false);
  const [triggerTimeout, setTriggerTimeout] = useState(false);
  const [newWarning, setNewWarning] = useState("");

  const handleTestifying = comment_id => {
    setDisplayPrompt(false);
    const url = `/offenders/confirm_comment/${comment_id}`;
    try {
      axios.get(url).then(resp => {
        if (resp.data.status === "success") {
          triggerCommentsReload(offender_id);
        }
      });
    } catch (e) {
      console.log(e);
    }
  };

  const handleNewWarning = async e => {
    e.preventDefault();
    if (newWarning.length < 25) {
      setDisplayCharacterLengthWarning(true);
    } else if (
      newWarning.includes("fuck") ||
      newWarning.includes("suck") ||
      newWarning.includes("f**k") ||
      newWarning.includes("s**k") ||
      newWarning.includes("shit") ||
      newWarning.includes("s**t") ||
      newWarning.includes("sh*t") ||
      newWarning.includes("fu*k") ||
      newWarning.includes("ass") ||
      newWarning.includes("asshole") ||
      newWarning.includes("a**hole") ||
      newWarning.includes("jerk")
    ) {
      setProfanityTrigger(true);
    } else {
      const url = "/offenders/submit_new_comment";
      const fd = new FormData();
      fd.append("new_comment", newWarning);
      fd.append("offender_id", offender_id);
      fd.append("user_id", sessionUsernameID);
      try {
        const resp = await axios.post(url, fd);
        if (resp) {
          if (resp.data.status === "success") {
            setNewWarningNotification(true);
            setNewWarning("");
            setTriggerTimeout(true);
          }
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  useEffect(() => {
    console.log("offender id  " + offender_id);
    if (newWarningNotification) {
      const timeout = setTimeout(() => {
        setNewWarningNotification(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [newWarningNotification]);

  useEffect(() => {
    if (triggerTimeout) {
      const timeout = setTimeout(() => {
        setTriggerTimeout(false);
        triggerCommentsReload(offender_id);
      }, 6000);
    }
    return () => clearTimeout();
  }, [triggerTimeout]);

  useEffect(() => {
    if (profanityTrigger) {
      const timeout = setTimeout(() => {
        setProfanityTrigger(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [profanityTrigger]);

  useEffect(() => {
    if (displayCharacterLengthWarning) {
      const timeout = setTimeout(() => {
        setDisplayCharacterLengthWarning(false);
      }, 5000);
    }
    return () => clearTimeout();
  }, [displayCharacterLengthWarning]);

  useEffect(() => {
    let name = document.getElementById("name_to_check").innerHTML;
    if (name.includes("&amp;#39;")) {
      let editedName = name.replace("&amp;#39;", "'");
      document.getElementById("name_to_check").innerHTML = editedName;
    }
    if (confirmedComments.length !== 0) {
      const timeout = setTimeout(() => {
        let str = document.getElementById("comment_to_check").innerHTML;
        if (str.includes("&amp;#39;")) {
          let res = str.replace("&amp;#39;", "'");
          document.getElementById("comment_to_check").innerHTML = res;
        }
      }, 50);
    }
  }, [confirmedComments]);

  useEffect(() => {
    if (
      confirmedComments !== 0 ||
      awaitingConfirmation !== 0 ||
      triggerNotification
    ) {
      document.getElementsByTagName("body")[0].style = "height: auto";
    }
  }, [confirmedComments, awaitingConfirmation, triggerNotification]);

  useEffect(() => {
    if (sessionUsernameID) {
      if (awaitingConfirmation.length !== 0 && !displayPrompt) {
        const timeout = setTimeout(() => {
          let str = document.getElementById("awaiting_comment_to_check")
            .innerHTML;
          if (str.includes("&amp;#39;")) {
            let res = str.replace("&amp;#39;", "'");
            document.getElementById(
              "awaiting_comment_to_check"
            ).innerHTML = res;
          }
        }, 50);
      }
    }
  }, [sessionUsernameID]);

  return (
    <>
      <div className="columns" style={{ marginTop: "30px" }}>
        <div className="column is-one-quarter">
          <div className="container" style={{ marginLeft: "30px" }}>
            <img
              src={firstPhoto}
              alt="first picture of offender"
              className="match_pictures"
              style={{ borderRadius: "10px" }}
            />
            <br />
            <img
              src={secondPhoto}
              alt="second picture of offender"
              className="match_pictures"
              style={{ borderRadius: "10px" }}
            />
            <br />
            <img
              src={thirdPhoto}
              alt="third picture of offender"
              className="match_pictures"
              style={{ borderRadius: "10px" }}
            />
          </div>
        </div>
        <div className="column">
          {triggerNotification && (
            <div className="notification is-warning">
              <p style={{ color: "#cc3300", fontWeight: "bold" }}>
                FOUND MATCH !
              </p>
            </div>
          )}

          <section className="section is-medium" style={{ paddingTop: "0" }}>
            <h4 id="name_to_check" className="title">
              {firstName}&nbsp;&nbsp;{middleName}&nbsp;&nbsp;{lastName}
            </h4>
          </section>
          <section className="section is-medium" style={{ padding: "0" }}>
            {confirmedComments.map((comm, index) => (
              <>
                <div
                  className="box"
                  key={index}
                  style={{ backgroundColor: "#ffcccc" }}
                >
                  <small>{comm.timestamp.slice(4, 16)}</small>
                  <br />
                  <br />
                  <h4 id="comment_to_check" className="title is-4">
                    {comm.post}
                  </h4>
                </div>
              </>
            ))}
          </section>
        </div>
        <div className="column is-one-third">
          <button
            className="delete is-large"
            onClick={() => onSwitchBack()}
          ></button>
          {displayCharacterLengthWarning && (
            <div
              className="notification is-warning"
              style={{
                width: "90%",
                marginTop: "20px"
              }}
            >
              <p style={{ color: "#cc3300", fontWeight: "bold" }}>
                New Warning Needs To Be At Least 25 Characters Long.
              </p>
            </div>
          )}
          {profanityTrigger && (
            <div
              className="notification is-warning"
              style={{
                width: "90%",
                marginTop: "20px"
              }}
            >
              <p style={{ color: "#cc3300", fontWeight: "bold" }}>
                No Swear Words Are Allowed. Let's Keep Things Decent :)
              </p>
            </div>
          )}
          {newWarningNotification && (
            <div
              className="notification is-info"
              style={{
                color: "white",
                fontWeight: "bold",
                width: "90%",
                marginTop: "20px"
              }}
            >
              <p>Warning Is Now Waiting For Confirmation.</p>
            </div>
          )}
          {sessionUsername && (
            <>
              <div
                className="box"
                style={{
                  backgroundColor: "#ff9999",
                  width: "90%",
                  marginTop: "20px"
                }}
              >
                <form onSubmit={e => handleNewWarning(e)}>
                  <h6 className="title is-6">Add New Warning</h6>
                  <textarea
                    onChange={e => setNewWarning(e.target.value)}
                    className="input"
                    name="comment"
                    rows="4"
                    style={{ height: "100px" }}
                    placeholder="Started stalking me after breakup, blackmailed me, ..."
                    value={newWarning}
                  />

                  <input
                    type="submit"
                    style={{ marginTop: "10px", marginLeft: "80%" }}
                    className="button is-warning"
                    value="Add"
                    required
                  />
                </form>
              </div>
            </>
          )}

          {awaitingConfirmation.length !== 0 && (
            <>
              <div
                className="notification is-warning is-light"
                style={{ width: "90%", marginTop: "20px" }}
              >
                <strong>
                  Warning Awaiting Confirmation
                  <br />
                  (24 hours window):
                </strong>
                {awaitingConfirmation.map((comm, index) => (
                  <>
                    {!comm.confirmed && (
                      <>
                        {!displayPrompt && (
                          <>
                            <div
                              className="box"
                              key={index}
                              style={{ marginTop: "10px" }}
                            >
                              <small>{comm.timestamp.slice(4, 16)}</small>
                              <br />
                              <br />
                              <p id="awaiting_comment_to_check">{comm.post}</p>
                              {comm.author !== sessionUsernameID &&
                                sessionUsername && (
                                  <button
                                    className="button is-danger"
                                    style={{
                                      fontFamily: "Orbitron",
                                      marginTop: "10px"
                                    }}
                                    onClick={() => setDisplayPrompt(true)}
                                  >
                                    Testify
                                  </button>
                                )}
                            </div>
                          </>
                        )}
                        {displayPrompt && (
                          <>
                            <div
                              className="notification is-warning"
                              style={{ marginTop: "20px" }}
                            >
                              <strong>
                                Confirm if you're absolutely sure. This can't be
                                undone.
                              </strong>
                              <br />
                              <br />
                              <button
                                className="button is-danger"
                                style={{
                                  fontFamily: "Orbitron",
                                  marginRight: "10px"
                                }}
                                onClick={() => handleTestifying(comm.id)}
                              >
                                Confirm
                              </button>
                              <button
                                className="button is-dark"
                                style={{ fontFamily: "Orbitron" }}
                                onClick={() => setDisplayPrompt(false)}
                              >
                                Cancel
                              </button>
                            </div>
                          </>
                        )}
                      </>
                    )}
                  </>
                ))}
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default Profile;
