import React, { useState, useEffect } from "react";
import Footer from "./Footer";
import Navbar from "./Navbar";
import { useGlobalContext } from "./context";
import { Redirect } from "react-router-dom";

const Terms = () => {
  const { setTrace } = useGlobalContext();
  const [isBackBtnClicked, setBackBtnClicked] = useState(false);

  useEffect(() => {
    setTrace("/terms");
    document.getElementsByTagName("body")[0].style = "height: auto";
    document.getElementsByTagName("title")[0].innerHTML = "Offenderbook's Terms";
    document.getElementsByTagName("footer")[0].style =
      "width:100%;background-color:#f63c58;padding: 10px 24px";
    document
      .querySelector('meta[name="description"]')
      .setAttribute("content", "Terms & Conditions");
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      {isBackBtnClicked && <Redirect to="/" />}
      <Navbar />
      <div className="container" style={{ marginTop: "50px" }}>
        <h1
          style={{ color: "#e6e6e6", fontSize: "25px" }}
          className="title is-1"
        >
          TERMS AND CONDITIONS
          <button
            style={{ marginLeft: "80px" }}
            className="delete is-large"
            onClick={() => setBackBtnClicked(true)}
          ></button>
        </h1>
        <br />
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          By visiting and using www.offenderbook.com (hereinafter the
          “website”), you accept and agree to be bound by these Terms and
          Conditions including our Privacy Policy posted on the website and
          incorporated herein by reference. The term “you” refers to anyone who
          uses, visits and/or views the website. Offenderbook reserves the right
          to amend or modify these terms and conditions in its sole discretion
          at any time without notice and by using the website, you accept those
          amendments. It is your responsibility to periodically check the
          website for updates. Your continued use of the website after posting
          of any changes to our Terms and Conditions constitutes your acceptance
          of those changes and updates. You must not access or use the website
          if you do not wish to be bound by these Terms and Conditions.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          Intended Age
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          All information and content on this website are intended for
          individuals over the age of 15. Children, as defined in our Privacy
          Policy, are prohibited from using this website.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          MANDATORY ARBITRATION AND GOVERNING LAW
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          You expressly waive your right to bring any legal claims, now or in
          the future arising out of or related to the website and our
          products/services. In the event of any dispute, claim or controversy
          arising out of or relating to your use of this website, the terms and
          conditions shall be construed in accordance with the rules and
          regulations of the UK and the United States. You agree to consent and
          submit to the jurisdiction of the state and federal courts located in
          the UK without regard to the principles of conflict of law or where
          the parties are located at the time a dispute arises. You agree to
          resolve any disputes or claims first through mandatory arbitration in
          the UK and shall bear the full cost of arbitration as permitted by
          law. Your good faith participation in arbitration is a condition
          precedent to pursuing any other legal or equitable remedies available
          such as litigation or any other legal procedure. You also agree that
          in the event a legal claim is initiated after the required
          arbitration, the prevailing party shall be entitled to recover
          reasonable attorney’s fees and other costs associated with the legal
          action.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          INTELLECTUAL PROPERTY{" "}
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          All content on this website including but not limited to text, posts,
          logos, marks, graphics, files, materials, services, products, videos,
          audio, applications, computer code, designs, downloads and all other
          information here (collectively, the “Content”) is owned by us and is
          protected by copyright, trademark and other intellectual property and
          unfair competition laws with the exception of any content from others
          that we are lawfully permitted to use. You are granted a limited
          revocable license to print or download Content from the website for
          your own personal, non-commercial, non-transferrable, informational
          and educational use only while ensuring it’s not in violation of any
          copyright, trademark, and intellectual property or proprietary rights.
          You agree not to copy, duplicate, steal, modify, publish, display,
          distribute, reproduce, store, transmit, post, create derivative works,
          reverse engineer, sell, rent or license any part of the Content in any
          way to anyone, without our prior written consent. You agree to abide
          by the copyright, trademark laws and intellectual property rights and
          shall be solely responsible for any violations of these terms and
          conditions.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          USER CONTENT AND LAWFUL USE OF THE WEBSITE
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          For any Content or information that you upload, display, post,
          transmit, send, email or submit to us on the website or on any of our
          social media sites, you warrant that you are the owner of that Content
          or have express permission from the owner of those intellectual
          property rights to use and distribute that Content to us. You grant us
          and/or our officers, employees, successors, shareholders, joint
          venture partners or anyone else working with us a royalty-free,
          perpetual, irrevocable, worldwide, non-exclusive right and license to
          identify you, publish, post, reformat, copy, distribute, display,
          edit, reproduce any Content provided by you on our website and on any
          of our social media sites for any purpose. You shall be solely liable
          for any damages resulting from any infringement of copyrights,
          trademark or other proprietary rights of any Content or information
          that you provide to us. You agree not to upload, display, post,
          transmit, distribute, send, email or submit to us on the website or on
          any of our social media sites any information or Content that is- (a)
          illegal, violates or infringes upon the rights of others, (b)
          defamatory, abusive, profane, hateful, vulgar, obscene, libelous,
          pornographic, threatening, (c) encourages or advocates conduct that
          would constitute a criminal offense, giving rise to civil liability or
          otherwise violate any law, (d) distribute material including but not
          limited to spyware, computer virus, any kind of malicious computer
          software or any other harmful information that is actionable by law,
          (e) any attempts to gain unauthorized access to any portion or feature
          of the website, and (f) send unsolicited or unauthorized material or
          cause disruption in the operation of the website. You agree to use the
          website for lawful purposes only and shall be liable for damages
          resulting from the violation of any provision contained in these Terms
          and Conditions.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          THIRD-PARTY LINKS
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          The website may contain links to third-party websites or resources for
          your convenience. We may serve as an affiliate for some of these
          third-party websites by offering or advertising their products or
          services on the website; however, we do not own or control these
          third-party websites. Once you click on a third-party link and leave
          this website, you are no longer bound by our terms and conditions. You
          agree that we are not responsible or liable for the accuracy, content
          or any information presented on these third-party websites. You assume
          all risks for using these third-party websites or resources and any
          transactions between you and these third-party websites are strictly
          between you and the third party. We shall not be liable for any
          damages resulting from your use of these third-party websites or
          resources.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          USE OF OUR PAID AND FREE PRODUCTS{" "}
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          We may offer free products for you to download and also sell paid
          courses, programs, physical or digital products and any other related
          materials (collectively, “products”) on this website. All our products
          and/or services including all content are protected by copyright
          pursuant to the US and international copyright laws. You are granted a
          limited revocable license to print or download Content from our
          digital products for your own personal, non-commercial,
          non-transferrable, informational and educational use only while
          ensuring it’s not in violation of any copyright, trademark, and
          intellectual property or proprietary rights. Copying or storing our
          content for other than personal use is expressly prohibited without
          our prior written consent. You acknowledge and agree that you have no
          right to share, modify, sell, edit, copy, reproduce, create derivative
          works of, reverse engineer, enhance or in any exploit our products.
          You cannot sell or redistribute any of our products, whether free or
          paid ones, without our express written consent. You agree to abide by
          the copyright, trademark laws and intellectual property rights and
          shall be solely responsible for any violations of these terms and
          conditions.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          TERMINATION
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          We reserve the right in our sole discretion to refuse, remove,
          restrict your access, revoke and terminate your use of our website
          including any or all Content published by you or us at any time for
          any reason, without notice.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        ></h3>
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          NO WARRANTIES{" "}
        </h3>
         
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          ALL CONTENT, INFORMATION, PRODUCTS AND/OR SERVICES ON THE WEBSITE ARE
          “AS IS” AND “AS AVAILABLE” BASIS WITHOUT ANY REPRESENTATIONS OR
          WARRANTIES OF ANY KIND INCLUDING THE WARRANTIES OF MERCHANTABILITY OR
          FITNESS FOR ANY PURPOSE, EXPRESS OR IMPLIED TO THE FULL EXTENT
          PERMISSIBLE BY LAW. COMPANY MAKES NO REPRESENTATIONS OR WARRANTIES AS
          TO THE CONTENT, INFORMATION, MATERIALS, PRODUCTS AND/OR SERVICES
          PROVIDED ON THIS WEBSITE. COMPANY MAKES NO WARRANTIES THAT THE WEBSITE
          WILL PERFORM OR OPERATE TO MEET YOUR REQUIREMENTS OR THAT THE
          INFORMATION PRESENTED HERE WILL BE COMPLETE, CURRENT OR ERROR-FREE.
          COMPANY DISCLAIMS ALL WARRANTIES, IMPLIED AND EXPRESS FOR ANY PURPOSE
          TO THE FULL EXTENT PERMITTED BY LAW.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          LIMITATION OF LIABILITY
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          You agree that under no circumstances, we and/or our officers,
          employees, successors, shareholders, joint venture partners or anyone
          else working with us shall be liable for any direct, indirect,
          incidental, consequential, equitable, special, punitive, exemplary or
          any other damages resulting from your use of this website including
          but not limited to all the content, information, products, services
          and graphics presented here. You expressly agree that your use of the
          website is at your sole risk and that you are solely responsible for
          the accuracy of the personal and any information you provide, the
          outcome of your actions, personal and business results, and for all
          other use in connection with the website. You also expressly agree
          that we and/or our officers, employees, successors, shareholders,
          joint venture partners or anyone else working with us shall not be
          liable to you for any damages resulting from 1) any errors or
          omissions on the website, delay or denial of any products or services,
          failure of performance of any kind, interruption in the operation and
          your use of the website, website attacks including computer virus,
          hacking of information, and any other system failures; 2) any loss of
          income, use, data, revenue, profits, business or any goodwill related
          to the website; 3) any theft or unauthorized access by third party of
          your information from the website regardless of our negligence; and 4)
          any use or misuse of the information, products and/or services offered
          here. This limitation of liability shall apply whether such liability
          arises from negligence, breach of contract, tort or any other legal
          theory of liability. You agree that we provide no express or implied
          guarantees to you for the content presented here, and you accept that
          no particular results are being promised to you here.
        </p>
        <br />
        <br />
           
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          INDEMNIFICATION
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          You agree to indemnify and hold the Company and/or its officers,
          employees, successors, shareholders, joint venture partners or anyone
          else working with us harmless from all losses, claims, damages,
          demands, actions, suits, proceedings or judgments, including costs,
          expenses and reasonable attorneys fees ("Liabilities") assessed
          against or otherwise incurred by you arising, in whole or in part,
          from: (a) actions or omissions, whether done negligently or otherwise,
          by you, your agents, directors, officers, employees or
          representatives; (b) all your actions and use of the website including
          purchasing products and services; (c) violation of any laws, rules,
          regulations or ordinances by you; or (d) violation of any terms and
          conditions of this website by you or anyone related to you; e)
          infringement by you or any other user of your account of any
          intellectual property or other rights of anyone. The Company will
          notify you promptly of any such claims or liability and reserves the
          right to defend such claim, liability or damage at your expense. You
          shall fully cooperate and provide assistance to us if requested,
          without any cost, to defend any such claims.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          ENTIRE AGREEMENT
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          These Terms and Conditions along with our Privacy Policy and
          Disclaimer constitute the entire agreement between you and us with
          respect to this website. It supersedes all prior or contemporaneous
          communications, discussions, negotiations or proposals we may have had
          with you whether electronic, oral or written. A printed version of
          this entire agreement including the Privacy Policy and Disclaimer and
          of any notice given in electronic form shall be admissible in judicial
          or administrative proceedings with respect to this website to the same
          extent and given the same effect as other business contracts and
          documents kept and maintained in printed form.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          SEVERABILITY
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          If any provision in these Terms and Conditions is deemed by a court,
          regulatory authority or other public or private tribunal of competent
          jurisdiction to be invalid or unenforceable, such provision is deemed
          to have been omitted from this Agreement. The remainder of this
          Agreement remains in full force and effect, and is modified to any
          extent necessary to give such force and effect to the remaining
          provisions, but only to such extent.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          GENERAL DISCLAIMER
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          Men profiles on this website are displayed for information purposes
          only. This website does not intend to shame, disrespect or harm in any
          way any of the men added to the website. Sole purpose of the website
          is to prevent potential abusive relationship from beginning.
        </p>
        <br />
        <br />
        <h3
          style={{ color: "#e6e6e6", fontSize: "20px" }}
          className="title is-3"
        >
          CONTACT
        </h3>
        <p style={{ color: "#e6e6e6", fontSize: "17px" }}>
          For any questions, please contact us at info@offenderbook.com .
        </p>
      </div>
      <br />
      <br />
      <Footer />
    </>
  );
};

export default Terms;
