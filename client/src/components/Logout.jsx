import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useGlobalContext } from "./context";
import axios from "axios";

const Logout = () => {
  const {
    sessionUsername,
    sessionUsernameID,
    setSessionUsername,
    setSessionUsernameID,
    setJustLoggedOutNotif,
    setRefreshTrigger
  } = useGlobalContext();

  const handleLogout = async e => {
    e.preventDefault();
    const url = `/auth/logout/${sessionUsernameID}`;
    const headers = {
      "Access-Control-Allow-Origin": "*"
    };
    try {
      const resp = await axios.get(url, headers);
      if (resp.data.message === "successfully logged out") {
        setSessionUsername("");
        setSessionUsernameID("");
        localStorage.clear();
        setJustLoggedOutNotif(true);
      }
    } catch (err) {
      console.log(null);
    }
  };

  useEffect(() => {
    handleLogout();
  }, []);

  return <Redirect to="/login" />;
};

export default Logout;
