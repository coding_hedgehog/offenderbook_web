import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { AppProvider } from "./components/context";
import Bulma from "bulma";
import "./assets/stylesheet.css";
import App from "./App";
import Login from "./components/Login";
import Join from "./components/Join";
import ConfirmEmail from "./components/ConfirmEmail";
import ConfirmEmailMobile from "./components/ConfirmEmailMobile";
import ComputersOnly from "./components/ComputersOnlyWarning";
import Terms from "./components/Terms";
import Privacy from "./components/Privacy";
import Logout from "./components/Logout";
import Feedback from "./components/Feedback";
import About from "./components/About";

ReactDOM.render(
  <React.StrictMode>
    <AppProvider>
      <Router>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/login" component={Login} />
          <Route path="/join" component={Join} />
          <Route path="/logout" component={Logout} />
          <Route
            exact
            path="/confirm_email/:auth_string?"
            component={ConfirmEmail}
          />
          <Route
            exact
            path="/confirm_email_mobile/:auth_string?"
            component={ConfirmEmailMobile}
          />
          <Route path="/about" component={About} />
          <Route path="/computers_only" component={ComputersOnly} />
          <Route path="/feedback" component={Feedback} />
          <Route path="/terms" component={Terms} />
          <Route path="/privacy" component={Privacy} />
        </Switch>
      </Router>
    </AppProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

serviceWorker.unregister();
